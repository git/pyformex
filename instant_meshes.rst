..

Install 'Instant Meshes'
------------------------

Instant Meshes is a program that can create quality quad surfaces
from a TriSurface.

The automatic installation of Instant is not ready yet. You can
however install it yourself by doing the following:

- download the program from https://github.com/wjakob/instant-meshes.
  (comes as a zipped ready to run binary)
- unzip the downloaded file somewhere in an executable path, e.g.
  your ~/bin, if that is in your PATH environment
- rename or symlink the 'Instant Meshes' binary to 'instant-meshes'

In commands::

  $ wget https://instant-meshes.s3.eu-central-1.amazonaws.com/instant-meshes-linux.zip
  $ unzip instant-meshes-linux.zip
  $ mv 'Instant Meshes' ~/bin
  $ ln -s 'Instant Meshes' ~/bin/instant-meshes

Then run ``pyFormex --detect`` to see if pyFormex can find it.

# End
