#!/bin/bash
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##

#
# This script installs pyFormex. Run ./install.sh --help for usage.
#

usage() {
    cat<<EOF

Usage: $(basename $0) OPTIONS TARGET...

Install pyFormex from source (either a git source or a tar.gz release).

Targets:

  all: build and install pyFormex
  build: build the binary version but do not install it
  install: install the previously built binary version
  clean: remove the build directory (used in building the wheel)
  find: find existing pyformex installations

Options:

  -n, --dry-run : show what commands would be run, but do not actually
     execute them.

  -s, --single-version : install a single pyFormex version. This will allow
     only one version of pyFormex to exist in the target installation path.
     Without this option, multiple pyFormex versions can be installed in
     parallel and executed each by The default is to install multiple versions
     which can each be executed by their own executable named pyformex-VERSION.
     One of them can be made the default (using the -d option) and can then
     be executed as pyformex.
     The -s option implies the -d option. If pyformex installations exist in
     the target path, the user will be asked if they can be removed. If not
     allowed, the installation is aborted. The -S option is a variant that
     forces removal of existing installations without user confirmation.

  -S, --Single-version: force a single version install. This is like the -s
     option, but forces removal of existing pyformex installations without
     asking the user. Use with care. Useful for unattended installation.

  -d, --default : make the installed version the default
     pyformex version. This symlinks the pyformex-VERSION/pyformex package
     to pyformex and the \$BINDIR/pyformex-VERSION executable
     to \$BINDIR/pyformex. This option is implied by the -s or -S option.

  -i, --installdir=INSTALLDIR : the path where to install pyFormex.
     Normally, the install procedure will detect the correct installation
     path. Only use this if you need to use another path and you know what
     you're doing.

  -b, --bindir=BINDIR : the path where to link the executable script.
     This should normally be a directory that is in your PATH variable.
     The default is /usr/local/bin for the root user, and \$HOME/.local/bin
     for other users.

  -h, --help : display this help page and exit.

EOF
}

#
# Define constants and default values for variables
#
. RELEASE
PYFVER=pyformex-$RELEASE
PYTHON=${PYTHON:-python3}
ECHO=
SINGLE=
DEFAULT=
PREFIX=
SCHEME=
BINDIR=
BDIST=whl
BDISTCMD=bdist_wheel
BUILDDIR=build         # We can not change this with bdist_wheel command!
DISTDIR=build         # This is where the .whl ends up
INSTALLDIR=
USER="$(id -un)"

#
# helper functions
#

# Return 'false' if $1 is undefined
# Return 'true' if $1 is defined and first char is not upper case
# Return 'force' if $1 is defined and first char is upper case
is_defined() {
    if [ -z "$1" ]; then
	echo false
    elif [[ "${1::1}" == [A-Z] ]]; then
	echo force
    else
	echo true
    fi
}

# Find the installation path for a given scheme
# If the scheme exists, return its 'platlib' path
# If not, return the installation path of numpy
find_installdir() {
    $PYTHON <<EOF
import sysconfig
scheme = "$SCHEME"
if scheme in sysconfig.get_scheme_names():
    print(sysconfig.get_paths(scheme)['platlib'])
else:
    import os, numpy
    print(os.path.dirname(os.path.dirname(numpy.__file__)))
EOF
}

# Find installed pyformex versions in the INSTALLDIR
find_installed() {
    INSTALLED=$(ls -d $INSTALLDIR/pyformex/ $INSTALLDIR/pyformex-*/ 2>/dev/null)
    if [ -n "$INSTALLED" ]; then
	echo "Existing pyFormex installations in the target path:"
	echo "$INSTALLED"
	if [ -n "$SINGLE" ]; then
	    if [[ $SINGLE == 'S' ]]; then
		ans=y
	    else
	        ans=N
		read -p "Single-version install: Should I remove these? (N/y) " ans
	    fi
	    if [[ ${ans::1} == [yY] ]]; then
	        remove_installed
	    else
		echo "Aborting single version installation"
		exit
	    fi
	fi
    fi
}

# Remove existing installations
remove_installed() {
    echo "Removing existing installations"
    for pyfver in $INSTALLED; do
	echo "Removing $pyfver"
	if [ -f "$pyfver/installation" ]; then
	    echo "Getting installation details from $pyfver/installation"
	    . $pyfver/installation
	    $ECHO rm -f $BINDIR/$PYFORMEX_VERSION
	fi
	rm -rf "$pyfver"
    done
}

#
# functions defining the actions
#

do_build() {
    #$ECHO ${PYTHON} setup.py build -b $BUILDDIR
    $ECHO ${PYTHON} setup.py $BDISTCMD -d $DISTDIR
    $ECHO make -C pyformex/extra build
}

do_install() {
    echo "Installing in $INSTALLDIR and $BINDIR"
    EXECUTABLE=$BINDIR/$PYFVER
    $ECHO install -d $INSTALLDIR $BINDIR
    find_installed
    $ECHO unzip -o $DISTDIR/$PYFVER-*.$BDIST $PYFVER'/*' -d $INSTALLDIR
    # echo "Hardwire Python version $PYTHON in startup script"
    # [ -n "$ECHO" ] || sed -i "/PYTHON=/s|=.*|=$PYTHON|" $INSTALLDIR/$PYFVER/pyformex/pyformex
    echo "Linking executable $INSTALLDIR/$PYFVER/pyformex/pyformex to $EXECUTABLE"
    $ECHO ln -sfn $INSTALLDIR/$PYFVER/pyformex/pyformex $EXECUTABLE
    [ -n "$DEFAULT" ] && {
	echo "Making $PYFVER the default pyformex"
	$ECHO ln -sfn $PYFVER $BINDIR/pyformex
	$ECHO ln -sfn $PYFVER/pyformex $INSTALLDIR/pyformex
    }
    $ECHO make -C pyformex/extra install prefix=$PREFIX
    echo "Record installation"
    echo "Copy installation to $INSTALLDIR/$PYFVER"
    $ECHO install installation $INSTALLDIR/$PYFVER
    cat $INSTALLDIR/$PYFVER/installation
}

do_clean() {
    $ECHO rm -rf build
}

do_desktop() {
    NAME=pyformex-$VERSION
    [ -f $NAME.desktop -a -f $NAME.png ] || {
	[ -x "./create_desktop_file" ] && ./create_desktop_file
    }
    [ -f $NAME.desktop -a -f $NAME.png ] || {
	echo "No desktop file or icon found."
	return 1
    }
    if [ "$USER" = "root" ]; then
	OPTION=-m
    else
	OPTION=-a
    fi
    $ECHO ./install_desktop_file $OPTION $NAME
}

do_all() {
    do_build
    do_install
    do_desktop
}

################################
# Process command line arguments
#
# Execute getopt

ARGS=$(getopt -o "nsSdei:b:h" -l "dry-run,single-version,Single-version,default,egg,installdir:,bindir:,help" -n "$(basename $0)" -- "$@")

#Bad arguments
[ $? -eq 0 ] || { usage; exit 1; }

eval set -- $ARGS

while [ "$1" != "--" ]; do
    case "$1" in
	-n|--dry-run) ECHO=echo ;;
	-s|-S|--single-version|--Single-version) SINGLE="${1:1:1}" ;;
	-d|--default) DEFAULT=1 ;;
	-i|--installdir) INSTALLDIR="$2"; shift ;;
	-b|--bindir) BINDIR="$2"; shift ;;
	-h|--help) usage; exit;;
	*) echo "Unknown option: $1" >&2; exit 1 ;;
   esac
   shift  # delete "$1"
done
shift  # delete the "--"

# Fill in defaults for missing required values
[ -n "$SINGLE" ] && DEFAULT=1
if [ "$USER" = "root" ]; then
    PREFIX=/usr/local
    SCHEME=posix_local
else
    PREFIX=$HOME/.local
    SCHEME=posix_user
fi
[ -n "$BINDIR" ] || BINDIR=$PREFIX/bin
[ -n "$INSTALLDIR" ] || INSTALLDIR=$(find_installdir)
[ -n "$INSTALLDIR" ] || {
    echo "I could not find the correct installation path."
    echo "You may need to fix your installation."
    echo "Or use the --installdir option."
    exit 1
}

# Installation parameters: this should be valid SHELL and PYTHON
cat <<EOF | tee installation
# pyFormex installation details
PYFORMEX_VERSION='$PYFVER'
PYTHON_VERSION='$($PYTHON --version)'
USER='$USER'
SINGLE_VERSION='$(is_defined $SINGLE)'
IS_DEFAULT='$(is_defined $DEFAULT)'
SCHEME='$SCHEME'
INSTALLDIR='$INSTALLDIR'
BINDIR='$BINDIR'
DIST_FORMAT='$BDIST'
EOF

# Check that we have targets
[ -z "$@" ] && {
    echo "** Nothing to be done: use -h for help"
    exit
}

# Execute the commands
for cmd in "$@"; do
    echo "** $cmd"
    case $cmd in
	build | install | desktop | all | clean) do_$cmd ;;
	find ) find_installed ;;
	* ) echo "No such command: $cmd"; exit 1 ;;
    esac
done


# End
