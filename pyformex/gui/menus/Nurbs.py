#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Nurbs menu

"""
import numpy as np
import pyformex as pf
from pyformex.gui import menu
from pyformex.plugins.nurbs import NurbsCurve
import pyformex.gui.guicore as pg
_I = pg._I

class _options:
    color = 'blue'
    ctrl = True
    ctrl_numbers = False
    knots = True
    knotsize = 6
    knot_numbers = False
    knot_values = False


def drawNurbs(N):
    pg.draw(N, color=_options.color, nolight=True)
    if _options.ctrl:
        pg.draw(N.coords, nolight=True)
        if _options.ctrl_numbers:
            pg.drawNumbers(N.coords, nolight=True)
    if _options.knots:
        pg.draw(N.knotPoints(), color=_options.color,
                marksize=_options.knotsize, nolight=True)
        if _options.knot_numbers:
            pg.drawNumbers(N.knotPoints(), nolight=True)


# Override some functions for nurbs

def createNurbsCurve(N, name=None):
    """Create a new Nurbs curve in the database.

    This will ask for a name if none is specified.
    The curve is exported under that name, drawn and selected.

    If no name is returned, the curve is not stored.
    """
    an = pf.autoName('nurbscurve')
    drawNurbs(N)
    if name is None:
        name = an.peek()
        res = pg.askItems([
            _I('name', name, text='Name for storing the object'),
        ])
        if not res:
            return None

    name = res['name']
    if name == an.peek():
        next(an)
    pm = pf.pmgr()
    pm.proj[name] = N
    pm.proj.set_filter(clas=NurbsCurve)
    # pm.refresh()  # Needed?
    pm.selection = [name]
    return name


def createInteractive():
    mode = 'nurbs'
    res = pg.askItems([('degree', 3), ('closed', False)])
    Draw2d.obj_params.update(res)
    print("z value = %s" % Draw2d.the_zvalue)
    points = Draw2d.draw2D(mode='point', npoints=-1, zvalue=Draw2d.the_zvalue,
                           preview=Draw2d.obj_params.get('preview', False))
    if points is None:
        return
    print("POINTS %s" % points)
    N = Draw2d.drawnObject(points, mode=mode)
    pf.canvas.removeHighlight()
    if N:
        createNurbsCurve(N, name=None)


def fromControlPolygon():
    if not selection_PL.check(single=True):
        selection_PL.ask(single=True)

    if selection_PL.check(single=True):
        n = selection_PL.names[0]
        C = pf.PF[n]
        res = pg.askItems([('degree', 3)])
        N = NurbsCurve(C.coords, degree=res['degree'])
        createNurbsCurve(N, name=None)
        pg.draw(C)


def fromPolyLine():
    if not selection_PL.check(single=True):
        selection_PL.ask(single=True)

    if selection_PL.check(single=True):
        n = selection_PL.names[0]
        C = pf.PF[n]
        N = NurbsCurve(C.coords, degree=1, blended=False)
        createNurbsCurve(N, name=None)


################################## Menu #############################

def create_menu(before='help'):
    """Create the menu."""
    MenuData = [
        ("&Select drawable", selection.ask),
        ("&Set grid", Draw2d.create_grid),
        ("&Remove grid", Draw2d.remove_grid),
        ("---", None),
        ("&Create Nurbs Curve ", [
            ("Interactive", createInteractive),
            ("From Control Polygon", fromControlPolygon),
            ("Convert PolyLine", fromPolyLine),
            # ("Convert BezierSpline",fromBezierSpline),
        ]),
        ("---", None),
    ]
    w = menu.Menu('Nurbs', items=MenuData, parent=pf.GUI.menu, before=before)
    return w


# End
