#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Menus for the pyFormex GUI.

This modules implements specialized classes and functions for building
the pyFormex GUI menu system.
"""
import pyformex as pf
from pyformex.gui import guiscript as pg
from pyformex.gui import toolbar

# The menu actions can be simply function names instead of strings, if the
# functions have already been defined here.

_gui_geometry = None

def saveGeometry():
    global _gui_geometry
    _gui_geometry = pf.GUI.saveGeometry()

def restoreGeometry():
    pf.GUI.restoreGeometry(_gui_geometry)

def printwindow():
    # pf.app.syncX()
    r = pf.GUI.XGeometry()
    print(f"Qt geom(w,h,x,y): {r}")

def printbbox():
    print(pf.canvas.bbox())

def closeLogFile():
    if pg.logfile:
        pg.logfile.close()
        pg.logfile = None

def openLogFile():
    fn = pg.askFilename(filter=['*.log', '*'])
    if fn:
        closeLogFile()
        pg.logfile = open(fn, 'w')


def printSysPath():
    import sys
    print(sys.path)


def printModules():
    import sys
    print(sorted(sys.modules.keys()))


def printLastCommand():
    """Print a nice report of the outcome of the last command"""
    print('=' * 8 + " LAST COMMAND REPORT " + '=' * 21)
    print(pf.utils.last_command)
    print('=' * 50)


def saveBoard():
    extra = [pg._I('contents', choices=['all', 'commands', 'output'])]
    res = pg.askFile(filter=['*.txt', '*'], extra=extra)
    if res:
        pf.GUI.console.saveoutput(res['filename'], res['contents'])


def clearBoard():
    pf.GUI.console.clear()


def showHistory():
    text = '\n'.join(pf.GUI.console.editline.history)
    pf.showText(text, mono=True)


def toggleConsole(onoff=None):
    if pf.GUI.console:
        if onoff is None:
            onoff = not pf.GUI.console.isVisible()
        pf.GUI.console.setVisible(onoff)
        pf.GUI.update()


def toggleConsoleInput(onoff=None):
    if pf.GUI.console:
        if onoff is None:
            onoff = not pf.GUI.console.inpedit.isVisible()
        pf.GUI.console.promptdisp.setVisible(onoff)
        pf.GUI.console.inpedit.setVisible(onoff)


########## The menu ##########

menu_items = [
    ('Play', pg.play),
    ('Rerun', pg.replay),
    ## ('Step',pg.step),
    ('Continue', pg.fforward),
    ('Stop', pf.script.raiseExit),
    ("---", None),
    ('Console', [
        ('Save Output', saveBoard),
        ('Clear Output', clearBoard),
        ('Show History', showHistory),
        ('Toggle Console', toggleConsole),
        ('Toggle Input Area', toggleConsoleInput),
    ]),
    ("---", None),
    ('Open Log File', openLogFile),
    ('Close Log File', closeLogFile),
    ('Print Config', pf.script.printConfig),
    ('Print Last Command', printLastCommand),
    ('Print Loaded Apps', pf.script.printLoadedApps),
    ('Print sys.path', printSysPath),
    ('Print loaded modules', printModules),
    ('Print Scene Bbox', printbbox),
    ('Print Viewport Settings', pg.printviewportsettings),
    ('Print Window Geometry', printwindow),
    ## ('Reset Picking Mode',resetPick),
    ('Save Geometry', saveGeometry),
    ('Restore Geometry', restoreGeometry),
    ('Toggle Input Timeout', toolbar.timeout),
    ('Reset GUI', pg.resetGUI),
]

# End
