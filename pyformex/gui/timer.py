from pyformex.gui import QtCore

class Timer(QtCore.QTimer):
    """A wrapper class over the :class:`QtCore.QTimer`.

    The Timer waits a given amount of time and then executes some code.

    Parameters
    ----------
    sec: float
        Time in seconds to wait before running *func*.
    func: callable
        Function to execute after *sec* seconds.
    repeat: bool
        If True, the execution of *func* will be repeated every *sec*
        seconds. The default is to execute only once.
    start: bool
        If True (default), the Timer is started immediately when created.
        It may be set to False to create a Timer that will be started
        later.

    Notes
    -----
    Keep the Timer bound to a variable, so that it is not destroyed before
    it has executed. QTimer methods like start, stop can be used to (re)start
    or stop an existing Timer.
    """
    def __init__(self, sec, func, repeat=False, start=True):
        """Initialize a Timer"""
        super().__init__()
        self.setInterval(int(1000 * sec))
        self.setSingleShot(not repeat)
        self.timeout.connect(func)
        if start:
            self.start()
