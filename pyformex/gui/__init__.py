#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""pyFormex GUI module initialization.

This module is intended to form a single access point to the Python
wrappers for the QT libraries, which form the base of the pyFormex GUI.
By using a single access point, we can better deal with API changes
in the wrappers.

All pyFormex modules accessing QT libraries should do this by importing
from this module.

This module also detects the underlying windowing system.
Currently, the pyFormex GUI is only guaranteed on X11.
For other systems, a warning will be printed that some things may not work.
"""
import sys
import pyformex as pf

known_bindings = {
    'pyside2': '>= 5.15',
    'pyqt5': '>= 5.15',
    'pyside6': '',
    'pyqt6': '',
}
if sys.version_info.minor >= 12:
    del known_bindings['pyside2']  # PySide2 segfaults with Python 3.12

# bindings = pf.options.bindings
# if not bindings:
bindings = pf.cfg['gui/bindings'].lower()

if bindings in known_bindings:
    # Force use of thiese bindings
    pf.Module.require(bindings, known_bindings[bindings])
else:
    # Try using any existing bindings
    bindings = ''
    for b in known_bindings:
        if pf.Module[b].detect():
            bindings = b
            pf.Module.require(bindings, known_bindings[bindings])
            break

# print(f"USING {bindings=}")

# Import the proper bindings: import these modules only from here!
if bindings == 'pyside2':
    from PySide2 import QtCore, QtGui, QtOpenGL, QtWidgets    # noqa: F401
elif bindings == 'pyqt5':
    from PyQt5 import QtCore, QtGui, QtOpenGL, QtWidgets    # noqa: F401
elif bindings == 'pyside6':
    from PySide6 import QtCore, QtGui, QtOpenGL, QtWidgets, \
        QtOpenGLWidgets  # noqa: F401
elif bindings == 'pyqt6':
    from PyQt6 import QtCore, QtGui, QtOpenGL, QtWidgets, \
        QtOpenGLWidgets  # noqa: F401
else:
    raise ValueError("\n" + "*"*40 + """
The pyFormex GUI requires Python3 bindings for the Qt5 or Qt6 libraries.
They are available as PySide2, PyQt5, PySide6 or PyQt6, but I couldn't find
either of them, so I can not continue.
Install one of these bindings and then try again.
If it comes in several subpackages, install at least the
QtCore, QtGui, QtOpenGL and QtWidgets components.
""" + "*"*40)

# Fix some uncompatible naming: import these names only from here!
if bindings[:6] == 'pyside':
    Signal, Slot = QtCore.Signal, QtCore.Slot
else:
    Signal, Slot = QtCore.pyqtSignal, QtCore.pyqtSlot


if bindings[-1:] >= '6':
    OpenGLWidget = QtOpenGLWidgets.QOpenGLWidget
    QAction = QtGui.QAction
else:
    # OpenGLWidget = QtWidgets.QOpenGLWidget
    OpenGLWidget = QtOpenGL.QGLWidget
    QAction = QtWidgets.QAction


if bindings == 'pyside2':
    def QByteArrayToStr(self):
        """Convert a QByteArray to a Python3 str"""
        return str(self)[2:-1]
    QtCore.QByteArray.toStr = QByteArrayToStr


# If we get here, either PySide2 or PyQt5 are imported
# Check for OpenGL and PIL
try:
    if 'mgl' in pf.options and pf.options.mgl:
        pf.Module.has('moderngl', fatal=True)
    elif 'gl3' in pf.options and not pf.options.gl3:
        pf.Module.has('pyopengl', fatal=True)
except Exception as e:
    print(e)
if not pf._sphinx:
    pf.Module.has('pil', fatal=True)

# Some Qt wrappers

# Allow a Path to be passed instead of a str
class QPixmap(QtGui.QPixmap):
    def __init__(self, *args):
        if isinstance(args[0], pf.Path):
            args = (str(args[0]),) + args[1:]
        QtGui.QPixmap.__init__(self, *args)


class QImage(QtGui.QImage):
    def __init__(self, *args):
        if isinstance(args[0], pf.Path):
            args = (str(args[0]),) + args[1:]
        QtGui.QImage.__init__(self, *args)

# End
