#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""GeomFile

Test the GeomFile export and import

.. metadata
  :level: normal
  :topics: geometry
  :techniques: color, geomfile
"""
import pyformex as pf
from pyformex.examples.Cube import cube_quad
_name = pf.Path(__file__).stem


def run():
    colormode = [None, 'Single', 'Face', 'Full']
    n = len(colormode)
    obj = {}
    pf.layout(2*n, 4)
    for vp, color in enumerate(colormode):
        pf.viewport(vp)
        pf.clear()
        pf.reset()
        pf.smooth()
        pf.view('iso')
        obj[str(color)] = o = cube_quad(color)
        pf.draw(o)

    if pf.checkWorkdir():
        indir = pf.cfg['workdir']
    else:
        tmpdir = pf.TempDir()
        indir = tmpdir.path

    pf.writeGeometry(indir / 'test.pgf', obj)

    oobj = pf.readGeometry(indir / 'test.pgf')
    for vp, color in enumerate(colormode[:4]):
        pf.viewport(vp+n)
        pf.clear()
        pf.reset()
        pf.smooth()
        pf.view('iso')
        pf.draw(oobj[str(color)])


if __name__ == '__draw__':
    run()
# End
