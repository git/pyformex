#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ColorPick

This example shows a custom colormap and allows the user to pick a color
and retrieve its name and value. Currently it only

.. metadata
  :level: normal
  :topics: color
  :techniques: color, colormap, pick
"""
import math
import numpy as np
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem

twosquares = "\u2B1B" * 2 + ' '

def print_color(i):
    print(f"print_color {i=}")
    name = pf.color.colornames[i]
    c = pf.Color(colormap[i])
    pf.printc(twosquares, color=c, end='')
    print(c.report('GRW'), end='')
    print(f", {name!r}")

def report_color(self):
    try:
        i = self.picked[0][0]
        print_color(i)
    except Exception as e:
        print(e)
        pass

def simulate_pick(rect=None):
    """Create the events for picking the specified rectangle"""
    events = pf.canvas.mouse_click_events((200, 200), 'left')
    pf.canvas.events.extend(events)
    events = pf.canvas.mouse_click_events((240, 230), 'left')
    pf.canvas.events.extend(events)
    events = pf.canvas.mouse_click_events((200, 200), 'right')
    pf.canvas.events.extend(events)


def run():
    global colornames, colormap
    pf.clear()
    pf.flatwire()
    pf.view('front')
    res = pf.askItems([_I('coll', choices=pf.color.colorsets(), itemtype='hradio')])
    if not res:
        return
    cset = res['coll']
    colornames = pf.color.colornames(cset)
    colormap = pf.color.colorArray(list(pf.color.colorset(cset).values()))
    # maxlen = max(len(color) for color in colornames)
    ncolors = len(colornames)
    if cset == 'pf':
        nx = 8
        ny = math.ceil(ncolors / nx)
    else:
        ny = math.ceil(math.sqrt(ncolors / pf.canvas.aspect))
        nx = math.ceil(ncolors / ny)
    M = pf.Mesh(eltype='quad4').scale(pf.canvas.aspect, 0).subdivide(nx, ny)
    # Remove the cells > ncolors
    M = M.cselect(np.arange(ncolors, nx*ny))
    M.setProp('range')
    # iblack = colornames.index('black')
    pf.draw(M, colormap=colormap)
    if pf.GUI.runallmode:
        # Start an autopick function
        _ = pf.Timer(2, simulate_pick)
    pf.pick('element', func=report_color, prompt='Pick any color to show'
            ' its name and value (ESC or right mouse to stop)')


if __name__ == '__draw__':
    run()

# End
