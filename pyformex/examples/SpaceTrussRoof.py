#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Double Layer Flat Space Truss Roof

.. metadata
  :level: normal
  :topics: geometry
  :techniques: dialog, animation, color, import, connect, interpolate
"""
import pyformex as pf
_name = pf.Path(__file__).stem

roof = None


def createRoof(dx, ht, nx, ny, m=0, colht=0.):
    """Create a space truss roof.

    Parameters
    ----------
    dx: float
        The modular size of the square modules of the roof deck
    ht: float
        The height of the deck
    nx: int
        The number of bottom deck modules in x direction (should be even)
    ny: int
        The number of bottom deck modules in y direction (should be even)
    m: int
        The multiplicity of the columns: every m modules a column will be placed.
        If <=0, no columns are added.
    colht: float
        The height of the columns.

    Returns
    -------
    Formex:
        A Formex with the roof structure. If m > 0, the roof is supported by
        columns under every m-th node of the roof's circumference
    """
    # The bottom layer of the roof truss
    bot = (pf.Formex('l:1').replicm((nx, ny+1)) + pf.Formex('l:2').replicm(
        (nx+1, ny))).scale(dx).setProp(3)
    # The top layer of the roof truss
    top = (pf.Formex('l:1').replicm((nx+1, ny+2)) + pf.Formex('l:2').replicm(
        (nx+2, ny+1))).scale(dx).translate([-dx/2, -dx/2, ht]).setProp(0)
    # The diagonals
    T0 = pf.Formex(4*[[[0, 0, 0]]])  # 4 times the corner of the bottom deck
    T4 = top.select([0, 1, nx+1, nx+2])  # 4 nodes of corner module of top deck
    dia = pf.formex.connect([T0, T4]).replicm((nx+1, ny+1), (dx, dx)).setProp(1)
    # The full roof truss
    if m <= 0:
        return top + bot + dia
    # Add columns
    ncx = nx//m + 1  # number of columns in x-direction
    ncy = ny//m + 1  # and in y-direction
    col = (pf.Formex([[[0, 0, -colht], [0, 0, 0]]]).replicm((ncx, 2), (m, ny)) +
           pf.Formex([[[0, m, -colht], [0, m, 0]]]).replicm(
               (2, ncy-2), (nx, m))).scale([dx, dx, 1]).setProp(2)
    return top + bot + dia + col


def run():
    global roof
    pf.reset()
    pf.clear()
    pf.linewidth(1)
    pf.delay(1)

    if roof is None:
        roof = createRoof(1800., 1500., 14, 14, 2, 5600.)

    F = roof.rotate(-90, 0)  # put the structure upright
    pf.draw(F)
    pf.createView('myview1', (30., 0., 0.), True)
    pf.view('myview1', True)

    pf.setDrawOptions({'bbox': 'last'})
    for i in range(19):
        pf.createView('myview2', (i*10., 20., 0.), True)
        pf.view('myview2', True)
        pf.delay(0.1)

    # fly tru
    if pf.ack("Do you want to fly through the structure?"):
        totaltime = 10
        nsteps = 50
        # make sure bottom iz at y=0 and structure is centered in (x,z)
        F = F.centered()
        F = F.translate(1, -F.bbox()[0, 1])
        pf.clear()
        pf.linewidth(1)
        pf.draw(F)
        bb = F.bbox()
        # create a bottom plate
        B = pf.simple.rectangle(1, 1).swapAxes(1, 2).centered().scale(
            F.sizes()[0]*1.5)
        pf.smooth()
        pf.draw(B, color='slategrey')
        # Fly at reasonable height
        bb[0, 1] = bb[1, 1] = 170.
        ends = pf.Formex([[bb[0]]]).interpolate(pf.Formex([[bb[1]]]), [-0.5, 0.6])
        path = pf.Formex(ends.coords.reshape(-1, 2, 3)).subdivide(nsteps)
        pf.linewidth(2)
        pf.draw(path)
        steptime = float(totaltime)/nsteps
        pf.flyAlong(path, sleeptime=steptime)


if __name__ == '__draw__':
    run()

# End
