#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Optical Illusions

.. metadata
  :level: normal
  :topics: illustration, geometry
  :techniques: dialog, draw, persistence, random
  :acknowledgements: Tomas Praet
"""
import random
import numpy as np
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


################# Illusion definitions #####################

def ParallelLines():
    """Parallel Lines

    This illustration consists only of equally sized squares.
    Though the thus formed horizontal lines seem to converge,
    they are strictly parallel.
    """
    resetview([0.8, 0.8, 0.8])
    lines = pf.Formex([[[0, 0, 0], [14, 0, 0]]]).rep(13, step=1, dir=1)
    pf.draw(lines, color=[0.8, 0.8, 0.8], linewidth=1.0)
    F = pf.Formex('4:0123').rep(13, step=1)
    F.setProp([0, 7])
    F += F.translate([0.2, 1, 0]) + F.translate([0.4, 2, 0]) + F.translate([0.2, 3, 0])
    F = F.rep(3, step=4, dir=1)
    pf.draw(F)


def RotatingCircle():
    """Rotating Circle

    When staring at the cross in the middle,
    the disappearing magenta circles create the illusion of a green
    rotating circle. If you keep concentrating your gaze on the centre,
    the green circle wil seem to devour the magenta circle, up to a point
    where you no longer see the magenta circles.
    Blinking or changing your focus will immediately undo the effect.
    """
    resetview([0.8, 0.8, 0.8])
    ask = pf.askItems(items=[
        ('Number of circles', 18),
        ('Radius of circles', 1.0),
        ('Radius of the figure', 12),
        ('Number of rotations', 10),
        ('color of circles', [1.0, 0.40, 1.0]),
        ('Sleep time', 0.1),
        ('Zoom', 14.)
    ])
    if not ask:
        return
    N = ask['Number of circles']
    r = ask['Radius of circles']
    R = ask['Radius of the figure']
    n = ask['Number of rotations']
    col = ask['color of circles']
    sl = ask['Sleep time']
    sc = ask['Zoom']
    box=[[-sc, -sc, -sc], [sc, sc, sc]]
    pf.draw(pf.simple.shape('plus'), bbox=box)
    F = pf.simple.sector(r, 360., 1, 16).trl(0, R).rotate(90)
    circles = [F.rotate(-i*360/N) for i in range(N)]
    actors = pf.draw(circles, color=col)
    print(len(actors))
    with pf.busyCursor():
        j = 0
        actors[j].visible = False
        pf.canvas.update()
        for i in range(n*N):
            pf.sleep(sl)
            k = (j+1) % N
            actors[k].visible = False
            actors[j].visible = True
            j = k
            pf.canvas.update()


def SquaresAndCircles():
    """Squares And Circles

    When you look at one of the white circles, the other circles seem to
    change color. In fact, the color they appear to have is the same as
    the color of the squares. This color is generated randomly.
    Press 'show' multiple times to see the effect of the color of the squares.
    You may need to zoom to get an optimal effect.
    """
    resetview([0.6, 0.6, 0.6])
    B, H = 16, 16
    F = pf.Formex('4:0123').replicm((B, H), (1.2, 1.2))
    R = 0.2/np.sqrt(2.)
    G = pf.simple.sector(R, 360., 1, 16).translate(
        [1.1, 1.1, 0]).replicm((B-1, H-1), (1.2, 1.2))
    G.setProp(7)
    pf.draw(F, color=np.random.rand(3)/2)
    pf.draw(G)


def ShadesOfGrey():
    """Shades Of Grey

    Our perception of brightness is relative.
    Therefore, the figure on the left looks a little darker than the one
    on the right. The effect can be somewhat subtle though.
    """
    resetview('grey80')
    back = pf.Formex('4:0123').scale([8, 8, 1])
    back += back.translate([-8, 0, 0])
    back.setProp([0, 7])
    C = pf.simple.circle(a1=11.25).rotate(-90, 2).points()
    F = pf.Formex([[C[i], C[i+1], 2*C[i+1], 2*C[i]]
                   for i in range(0, 32, 2)]).translate([2, 4, 0])
    n = 100
    A = None
    pf.draw(back)
    for i in range(n):
        F = F.translate([-2./n, 0, 0])
        G = F.reflect(0)
        A = pf.draw(F+G, color='grey60', ontop=True, undraw=A)
        pf.sleep(5/n)


def RunningInCircles():
    """Running In Circles

    If you don't look directly at the rectangles, both rectangles will
    appear to 'overtake' each other constantly, although they are moving
    at equal and constant speed.
    """
    resetview()
    # box= [[-8, -8, -8], [8, 8, 8]]
    N = 72
    R = 10
    C = pf.simple.circle(a1=360./N).points()
    O =[0, 0, 0]
    F = pf.Formex([[C[i], C[i+1], O]
                   for i in np.arange(0, 2*N, 2)]).scale([R, R, 0])
    F.setProp([0, 7])
    # p = pf.simple.circle(a1=360./N).points()
    centre = pf.simple.polygonSector(N).rosette(N, 360./N).scale(R/3)
    centre.setProp(1)
    pf.draw(centre)
    pf.draw(F)
    pf.zoom(2.)
    b1 = pf.Formex('4:0123').scale([1.5, 0.8, 0]).translate([0, 8.5, 0.1])
    b1.setProp(3)
    b2 = pf.Formex('4:0123').scale([1.5, 0.8, 0]).translate([0, 7, 0.1])
    b2.setProp(6)
    b = b1+b2
    col = [np.random.rand(3)/3, [1, 1, 1]-np.random.rand(3)/8]
    A = None
    with pf.busyCursor():
        n = 8*N
        for i in range(n):
            b = b.rotate(4*360./n)
            A = pf.draw(b, color=col, undraw=A)


def HowManyColors():
    """How Many Colors

    How many colors are there in this image?
    It looks like there are 4 colors (pink, orange, light green and cyan),
    but in fact, there are only 3. The blueish and greenish colors are
    exactly the same.
    Lots of zooming might convince you that this is the case.
    """
    resetview()
    magenta = pf.Color('magenta')
    orange = pf.Color('orange')
    cyan = pf.Color('cyan')
    b, h, B, H = 10, 0.5, 11, 99
    F = pf.Formex('4:0123').scale([b, h, 1]).replicm((B, H), (b, h))
    col = np.resize(magenta, (H, B, 3))
    for i in range(H):
        for j in range(B):
            if i % 2 == 0:
                if j % 4 == 1:
                    col[i, j] = cyan
            else:
                if j % 4 == 3:
                    col[i, j] = cyan
                else:
                    col[i, j] = orange
    pf.draw(F, color=col.reshape(-1, 3))


def AlignedLines():
    """Aligned Lines

    This is a classic optical illusion.
    Straight lines can appear to be shifted when only a tilted part is
    visible.
    """
    resetview()
    a = 60.
    lines = pf.Formex('l:1').scale([20, 1, 0]).rotate(a).translate(
        [-20.*np.cos(a*np.pi/180.), 0, 0]).rep(32, step=1)
    lines = lines.cutWithPlane([-1, 0, 0], [1, 0, 0], side='+').cutWithPlane(
        [22, 0, 0], [1, 0, 0], side='-')
    mask = pf.Formex('4:0123').scale([1, 20.*np.sin(a*np.pi/180.),
                                      1]).rep(11, step=2)
    mask.setProp(6)
    savedelay = pf.GUI.drawwait
    pf.draw(mask, color=np.random.rand(3))
    pf.delay(2)
    pf.draw(lines, linewidth=2)
    for i in range(3):
        pf.wait()
        pf.renderMode('wireframe')
        pf.wait()
        pf.renderMode('flat')
    pf.delay(savedelay)


def ParallelLinesOverWheel():
    """Parallel Lines Over Wheel

    Another commonly seen illusion.
    The lines in the back tend to give the illusion of curved lined,
    although they are completely straight.
    Some zooming can help to optimize the effect.
    """
    resetview()
    C = pf.simple.sector(2, 360., 1, 16)
    pf.draw(C)
    line = pf.Formex([[[-20, 0, 0], [20, 0, 0]]])
    lines = line
    hor = line.translate([0, -4, 0]) + line.translate([0, 4, 0])
    pf.draw(hor, color='red', linewidth=4)
    for i in range(0, 180, 5):
        lines += line.rotate(i)
    pf.draw(lines, linewidth=1)


def MotionInducedBlindness():
    """Motion Induced Blindness

    This is a very nice illusion.
    Look at the centre of the image.
    The moving background will give the illusion that the other static points
    disappear.
    Blinking or changing your focus will immediately undo the effect.
    Cool huh?
    """
    resetview('black')
    ans = pf.askItems([
        _I('Number of static points', 10),
        _I('Background', None, itemtype='radio',
           choices=['Tiles', 'Structured points', 'Random points']),
        _I('Revolutions', 2),
        _I('Resolution', 180),
        _I('Number of random points', 300)])
    if not ans:
        return
    nr = ans['Number of random points']
    res = ans['Resolution']
    rot = ans['Revolutions']
    back = ans['Background']
    n = ans['Number of static points']
    pf.draw(pf.simple.shape('star').scale(2), color='red', linewidth=2)
    points = pf.Formex([[0, -10, 0]]).rosette(n, 360./n)
    pf.draw(points, color=np.random.rand(3), marksize=10)
    col = np.random.rand(3)
    trl = [-15., -15., 0.]
    if back=='Tiles':
        F = pf.simple.shape('plus').replicm((11, 11), (3, 3)).translate(trl)
    elif back=='Structured points':
        F = pf.Formex([[0, 0, 0]]).replicm((30, 30)).translate(trl)
    else:
        F = pf.Formex(np.random.rand(nr, 3)).scale([30, 30, 0]).translate(trl)
    a = 360. / res
    A = None
    pf.zoomAll()
    for i in range(rot*res):
        F = F.rotate(a)
        A = pf.draw(F, color=col, linewidth=2, bbox='last', undraw=A)


def FlickerInducedBlindness():
    """Flicker Induced Blindness

    """
    # ... STILL UNDER DEVELOPMENT...
    # (pyFormex might lack the possibility to reach the correct frequencies)
    resetview('black')
    n, freq, d = 4, 8., 0.17
    sl = 1/2/freq
    centre = pf.simple.shape('plus').scale(0.4)
    centre.setProp(7)
    pf.draw(centre)
    points = pf.Formex([[0, -5, 0]]).rosette(n, 360./n)
    points.setProp(6)
    pf.draw(points, color=[0.2, 0.5, 0.3], marksize=6)
    F1 = pf.Formex('4:0123').scale([1, 2, 1]).translate([d, -6, 0])
    F2 = F1.translate([-1-2*d, 0, 0])
    F1 = F1.rosette(n, 360./n)
    F2 = F2.rosette(n, 360./n)
    A1 = pf.draw(F1, color=[0.4, 0., 0.], visible=True)
    A2 = pf.draw(F2, color=[0.4, 0., 0.], visible=False)
    # A1.visible = True
    # A2.visible = False
    for i in range(int(50*freq)):
        pf.sleep(sl)
        A1.visible = not A1.visible
        A2.visible = not A2.visible
        pf.canvas.update()


def SineWave():
    """Sine Wave

    A pf.simple.yet powerful illusion: the vertical lines seem larger at places
    where the sine wave is more horizontal, and smaller where the sine wave
    is more vertical.

    Play with the parameters to get a more obvious result. Amplitude 0 shows
    that all lines are equally large.
    """
    resetview()
    res = pf.askItems([
        ('Amplitude', 3.5),
        ('Periods', 3),
        ('Spacing between lines', 0.1)
    ])
    if not res:
        return
    shift = res['Spacing between lines']
    per = res['Periods']
    amp = res['Amplitude']
    n = int(2*np.pi/shift*per)
    F = pf.Formex('l:2').rep(n, step=shift)
    for i in F:
        i[0, 1] = amp*np.sin(i[0, 0])
        i[1, 1] = i[0, 1] + 1
    pf.draw(F)


def CirclesAndLines():
    """Circles And Lines

    Another classic. All lines are completely straight, though the circles
    in the background give you the illusion that they aren't.
    The colors are generated randomly; some combination might not work as
    well as others.
    """
    resetview()
    n, m, nc = 5, 5, 8
    size = nc*np.sqrt(2)
    lines = pf.Formex('l:1234').translate([-0.5, -0.5, 0]).rotate(45).scale(size)
    c = pf.simple.circle(a1=5)
    C = c
    for i in range(2, nc+1):
        C += c.scale(i)
    C = C.replicm((n, m), (2*nc, 2*nc))
    lines = lines.replicm((n, m), (2*nc, 2*nc))
    pf.draw(C, linewidth=2, color=np.random.rand(3))
    pf.draw(lines, linewidth=3, color=np.random.rand(3))


def Crater():
    """Crater

    Though this image is 2D, you might get a 3D illusion.
    Look carefully and you'll see the whirlabout of a crater shaped object.
    """
    resetview()
    deg, rot, col = 5, 3, np.random.rand(2, 3)
    r1, r2, r3, r4, r5, r6, r7, r8 = 1, 1.9, 2.9, 4, 5.1, 6.1, 7, 7.8
    p = pf.simple.circle(a1=5).points()
    C = pf.PolyLine(p[0:len(p):2])
    C1 = C.scale(r1).translate([0, r1, 0])
    C2 = C.scale(r2).translate([0, r2, 0])
    C3 = C.scale(r3).translate([0, r3, 0])
    C4 = C.scale(r4).translate([0, r4, 0])
    C5 = C.scale(r5).translate([0, 2*r4-r5, 0])
    C6 = C.scale(r6).translate([0, 2*r4-r6, 0])
    C7 = C.scale(r7).translate([0, 2*r4-r7, 0])
    C8 = C.scale(r8).translate([0, 2*r4-r8, 0])
    fig = C1+C2+C3+C4+C5+C6+C7+C8
    A = None
    for i in range(rot*360//deg):
        fig = fig.rotate(deg)
        A = pf.draw(fig, color=col, undraw=A)


def Cussion():
    """Cussion

    This is a powerful illusion, though again some color combinations might
    not work as well as others.
    The smaller squares on this 'chessboard' tend to give a distortion.
    Again, all horizontal and vertical lines are perfectly parallel and
    straight!
    """
    resetview()
    b, h = 17, 17
    if b % 2 == 0:
        b += 1
    if h % 2 == 0:
        h += 1
    chess = pf.Formex('4:0123').replicm((b, h)).translate([-b/2+0.5, -h/2+0.5, 0])
    col=[np.random.rand(3), np.random.rand(3)]
    sq1 = pf.Formex('4:0123').scale([0.25, 0.25, 1]).translate([-0.45, 0.2, 0])
    sq2 = pf.Formex('4:0123').scale([0.25, 0.25, 1]).translate([0.2, -0.45, 0])
    F = (sq1.translate([1, 0, 0]).replic(int(b/2)-1, 1) +
         sq2.translate([0, 1, 0]).replic(int(h/2)-1, 1, dir=1))
    sq = sq1+sq2
    for i in range(int(b/2)):
        for j in range(int(h/2)):
            if i+j < (int(b/2)+int(b/2))/2-1:
                F += sq.translate([i+1, j+1, 0])
    colors = np.ndarray([0, 0])
    for i in F:
        if (int(i[0, 0]) + int(i[0, 1]) - 1) % 2 == 0:
            colors = np.append(colors, col[1])
        else:
            colors = np.append(colors, col[0])
    colors = colors.reshape(-1, 3)
    F = F.rosette(4, 90)
    pf.draw(F, color=colors.reshape(-1, 3))
    pf.draw(chess, color=col)


def CrazyCircles():
    """Crazy Circles

    You've undoubtably seen some variation of this illusion before.
    Looking at different spots of the image will give the illusion of motion.
    The secret is all in the combination of colors. Zooming might increase
    the effect.
    Switching to wireframe removes the effect and proves that this is merely
    an illusion.
    """
    resetview()
    n = 5*6
    col = [[1., 1., 1.], [0.12, 0.556, 1.], [0., 0., 1.], [0., 0., 0.],
           [0.7, 0.9, 0.2], [1., 1., 0.]]
    p = pf.simple.circle(a1=10).rotate(5).points()
    f = pf.Formex([p[0:len(p):2]])
    F = f.copy()
    for i in range(1, n):
        F += f.scale([1+i*0.5, 1+i*0.5, 1])
    F1 = F.replicm((5, 5), (n+1, n+1))
    F2 = F.replicm((4, 4), (n+1, n+1)).translate([(n+1)/2, (n+1)/2, 0])
    pf.draw(F1+F2, color=col)


############ Other actions #################

def resetview(bgcol='white'):
    pf.clear()
    pf.reset()
    pf.layout(nvps=1)
    pf.bgcolor(bgcol)
    pf.renderMode('flat')
    pf.perspective(False)
    pf.frontView()


############# Create dialog #################

dialog = None
explanation = None

functions = [
    ParallelLines,
    RotatingCircle,
    SquaresAndCircles,
    ShadesOfGrey,
    RunningInCircles,
    HowManyColors,
    AlignedLines,
    ParallelLinesOverWheel,
    MotionInducedBlindness,
    FlickerInducedBlindness,
    SineWave,
    CirclesAndLines,
    Crater,
    Cussion,
    CrazyCircles,
]
random.shuffle(functions)

keys = [f.__name__ for f in functions]
illusions = dict(zip(keys, functions))

# Dialog Actions

def close():
    """Close the dialog"""
    global dialog, explanation
    if dialog:
        dialog.close()
        dialog = None
    if explanation:
        explanation.close()
        explanation = None
    # Release script.ock
    pf.script.scriptRelease(__file__)


def explain():
    """Show the explanation"""
    global dialog, explanation
    if not dialog.validate():
        return
    res = dialog.results
    ill = res['Illusion']
    text = illusions[ill].__doc__
    Explain = res['Explain']
    print(text)
    if Explain:
        # use a persistent text box
        if explanation:
            explanation.updateText(text)
            explanation.raise_()
        else:
            # create the persistent text box
            # TODO: put in the main dialog
            explanation = pf.GUI.popupMessage(
                text, actions=['Close'], modal=False)
            explanation.raise_()
    else:
        # show a non-persistent text
        pf.showInfo(text)


def show():
    """Show the illusion"""
    if not dialog.validate():
        return
    ill = dialog.results['Illusion']
    # if Explain:
    #     explain()
    dialog.hide()
    illusions[ill]()
    dialog.show()


def shownext():
    """Show the next illusion"""
    if not dialog.validate():
        return
    ill = dialog.results['Illusion']
    i = keys.index(ill)
    i = (i + 1) % len(keys)
    dialog.updateData({'Illusion': keys[i]})
    show()


def showAll():
    """Show all the illusions"""
    dialog.hide()
    for ill in illusions:
        print(ill)
        illusions[ill]()
    dialog.show()


def timeOut():
    """What to do when the dialog receives a timeout signal"""
    showAll()
    pf.wait()
    close()


def openDialog():
    """Create and display the dialog"""
    global dialog, explanation, Explain

    dialog = pf.Dialog(caption=_name, store=_name+'_data', items=[
        _I('Illusion', choices=keys),
        _I('Explain', Explain, text='Show explanation'),
    ], actions=[
        ('Done', close), ('Next', shownext), ('Explain', explain), ('Show', show)
    ], default='Show')
    dialog.show(timeoutfunc=timeOut)


def run():
    global Illusion, Explain
    pf.script.scriptRelease(__file__)
    Illusion = None
    Explain = False
    print('close')
    close()
    print('open')
    openDialog()

    # Block other scripts
    pf.script.scriptLock(__file__)


if __name__ == '__draw__':
    run()

# End
