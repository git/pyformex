#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ConnectClosest

This example connects each point in a set with the closest other point.

.. metadata
  :level: beginner
  :topics: formex, mesh
  :techniques: connect, closest, regularGrid, addNoise
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    pf.clear()
    # Create a grid of points with
    X = pf.Coords(pf.simple.regularGrid(
        [0., 0., 0.], [1., 1., 0.], [3, 3, 0]).reshape(-1, 3))
    X = X.addNoise(rsize=0.05, asize=0.0)
    pf.draw(X)
    pf.drawNumbers(X)
    ind = pf.geomtools.closest(X)
    M = pf.formex.connect([X, X[ind]]).toMesh().removeDuplicate()
    # Hint: the pf.formex.connect() function transforms all items in the first
    # argument to Formices. Thus it also works directly with Coords objects.
    pf.draw(M)
    pf.drawNumbers(M, color='red')


if __name__ == '__draw__':
    run()
# End
