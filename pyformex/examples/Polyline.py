#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Polyline

Examples of PolyLine approximation of curves and coarsening of PolyLines.

.. metadata
  :level: normal
  :topics: geometry, curve
  :techniques: widgets, persistence, import, polyline, coarsening
"""
import pyformex as pf
from pyformex import _I, _G
_name = pf.Path(__file__).stem


def drawCurve(dset, closed, nseg, chordal, method, coarsen, tol, maxlen, refine,
              numbers):
    global S
    P = dataset[dset]
    S = pf.BezierSpline(P, closed=closed)

    if method == 'chordal':
        nseg = None

    PL = S.approx(nseg=nseg, chordal=chordal, equidistant=method=='equidistant')
    pf.draw(PL, color='red')
    pf.draw(PL.pointsOn(), color='black')
    if numbers:
        pf.drawNumbers(PL.pointsOn(), color='black')

    if coarsen:
        PC = PL.coarsen(tol, maxlen)
        print("Coarsened from %s to %s points" % (PL.npoints(), PC.npoints()))
        if refine:
            print(PC)
            PC = PC.refine(maxlen)
            print(PC)
        pf.draw(PC, color='blue')
        pf.draw(PC.pointsOn(), color='blue', marksize=10)


dialog = None
#
# TODO: closing the window (by a button) should also
#       call a function to release the scriptlock!
#
def close():
    global dialog
    if dialog:
        dialog.close()
        dialog = None
    # Release script.ock
    pf.script.scriptRelease(__file__)


def show():
    if not dialog.validate():
        return
    res = dialog.results
    usemaxlen = res.pop('usemaxlen')
    if not usemaxlen:
        res['maxlen'] = None
    res['dset'] = int(res['dset'])
    if res.pop('clear'):
        pf.clear()
    pf.setDrawOptions({'bbox': 'auto'})
    drawCurve(**res)
    pf.setDrawOptions({'bbox': None})


def timeOut():
    try:
        show()
    finally:
        close()


def run():
    global dialog, dataset

    pf.clear()
    pf.setDrawOptions({'bbox': 'auto', 'view': 'front'})
    pf.linewidth(2)
    pf.flat()

    dataset = [
        pf.Coords(((1., 0., 0.), (0., 1., 0.), (-1., 0., 0.), (0., -1., 0.))),
        pf.Coords(((6., 7., 12.), (9., 5., 6.), (11., -2., 6.), (9., -4., 14.))),
        pf.Coords(((-5., -10., -4.), (-3., -5., 2.), (-4., 0., -4.),
                   (-4., 5, 4.), (6., 3., -1.), (6., -9., -1.))),
        pf.Coords(((-1., 7., -14.), (-4., 7., -8.), (-7., 5., -14.),
                   (-8., 2., -14.), (-7., 0, -6.), (-5., -3., -11.),
                   (-7., -4., -11.))),
        pf.Coords(((-1., 1., -4.), (1., 1., 2.), (2.6, 2., -4.), (2.9, 3.5, 4.),
                   (2., 4., -1.), (1., 3., 1.), (0., 0., 0.), (0., -3., 0.),
                   (2., -1.5, -2.), (1.5, -1.5, 2.), (0., -8., 0.),
                   (-1., -8., -1.), (3., -3., 1.))),
        pf.Coords(((0., 1., 0.), (0., 0.1, 0.), (0.1, 0., 0.), (1., 0., 0.))),
        pf.Coords(((0., 1., 0.), (0., 0., 0.), (0., 0., 0.), (1., 0., 0.))),
        pf.Coords(((0., 0., 0.), (1., 0., 0.), (1., 1., 1.),
                   (0., 1., 0.))).scale(3),
    ]

    # parameters for drawCurve
    # dset, closed, nseg, chordal, method, coarsen, tol, maxlen, refine, numbers
    _items = [
        _I('dset', '0', choices=[str(i) for i in range(len(dataset))],
           text='Data set'),
        _I('closed', False, text='Close the curve'),
        _I('method', choices=['chordal', 'parametric', 'equidistant'],
           text='Approximation Method'),
        _I('chordal', 0.01, text="Chordal distance for approximation"),
        _I('nseg', 4, text="Number of segments"),
        _G('coarsen', [
            _I('tol', 0.01),
            _I('usemaxlen', False),
            _I('maxlen', 0.1),
            _I('refine', False),
        ], check=False),
        _I('clear', True),
        _I('numbers', True, text="Show numbers"),
    ]

    _enablers = [
        ('method', 'chordal', 'chordal'),
        ('method', 'parametric', 'nseg'),
        ('method', 'equidistant', 'nseg'),
        ('usemaxlen', True, 'maxlen', 'refine'),
    ]

    dialog = pf.Dialog(
        caption=_name, store=_name+'_data', items=_items,
        enablers=_enablers,
        actions=[('Close', close), ('Clear', pf.clear), ('Show', show)],
        default='Show')

    dialog.show(timeoutfunc=timeOut)
    # Block other scripts
    pf.script.scriptLock(__file__)


if __name__ == '__draw__':
    run()

# End
