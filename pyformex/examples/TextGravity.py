#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""TextGravity

Show the use of the text gravity parameter.

.. metadata
  :level: beginner
  :topics:
  :techniques: text
"""
import pyformex as pf
from pyformex.opengl.decors import Grid2D
from pyformex.opengl.textext import FontTexture
_name = pf.Path(__file__).stem


def run():
    pf.clear()
    pf.lights(False)
    x, y = pf.canvas.width()//2, pf.canvas.height()//2
    H = Grid2D(x-200, y-200, x+200, y+200, 2, 2, rendertype=2, color='red',
               linewidth=2)
    pf.drawActor(H)


    pf.delay(2)
    for g in ['NW', 'N', 'NE', 'W', 'C', 'E', 'SW', 'S', 'SE']:
        T = pf.drawText(f"XXX  {g}  XXX", (x, y), gravity=g, size=24)
        pf.wait()
        pf.undecorate(T)

    pf.delay(1)
    for f in pf.utils.listMonoFonts():
        print(f)
        font = FontTexture(f, 24)
        S = pf.drawText(f, (20, 20), font=font, size=24)
        T = pf.drawText('X', (x, y), font=font, size=48, gravity='')
        pf.wait()
        pf.undecorate(S)
        pf.undecorate(T)


if __name__ == '__draw__':
    run()

# End
