#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""IntersectionLineSurface

Find the intersection points of a set lines with a TriSurface.
This example teaches how to recognize the intersection points
with a specific line.

.. metadata
  :level: normal
  :topics: surface
  :techniques: intersection
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def run():

    pf.clear()
    pf.transparent()

    nquad = 1
    nsphere = 5

    S = pf.simple.sphere(nsphere)

    # Create the points to define the intersecting lines
    R = pf.simple.regularGrid([0., 0., 0.], [0., 1., 1.], [1, nquad, nquad])
    L0 = pf.Coords(R.reshape(-1, 3)).trl([-2., -1./2, -1./2]).fuse()[0]
    L1 = L0.trl([4., 0., 0.])

    P, X = S.intersectionWithLines(L0, L1, method='line', atol=1e-5)

    # Retrieve index of points, corresponding lines and triangles hit
    id_pts = X[:, 0]
    id_intersected_line = X[:, 1]
    id_hit_triangle = X[:, 2]

    hitsxline = pf.at.inverseIndex(id_intersected_line.reshape(-1, 1))
    Nhitsxline = (hitsxline>-1).sum(axis=1)

    ptsok = id_pts[hitsxline[np.where(hitsxline>-1)]]
    hittriangles = id_hit_triangle[hitsxline[np.where(hitsxline>-1)]]

    colors = ['red', 'green', 'blue', 'cyan']
    [pf.draw(pf.Formex([[p0, p1]]), color=c, linewidth=2, alpha=0.7)
     for p0, p1, c in zip(L0, L1, colors)]

    id=0
    for icolor, nhits in enumerate(Nhitsxline):
        for i in range(nhits):
            pf.draw(P[ptsok][id], color=colors[icolor], marksize=5, alpha=1)
            pf.draw(S.select([hittriangles[id]]), ontop=True, color=colors[icolor])
            id+=1
    pf.draw(S, alpha=0.3)


if __name__ == '__draw__':
    run()

# End
