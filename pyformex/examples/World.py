#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""World

This examples shows how to use an image file to set colors on a
structure.

The user can select an image file. A rectangular grid with corrersponding
number of cells is constructed. The grid is shown with the colors from the
image and can possibly be transformed to a sphere or a half sphere.

.. note::
  This example is slow when using high resolution images.

.. metadata
  :level: normal
  :topics: image
  :techniques: color, filename
"""
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


def run():
    pf.clear()
    pf.smooth()
    pf.lights(False)
    pf.view('front')

    # default image
    fn = pf.cfg['datadir'] / 'world.jpg'

    res = pf.askItems([
        _I('fn', fn, itemtype='file', filter='img', mode='exist', text=''),
        _I('part', itemtype='radio', choices=[
            "Plane", "Half Sphere", "Full Sphere"], text='Show image on'),
    ])
    if not res:
        return

    fn = res['fn']
    part = res['part']

    # Create the colors
    color, colormap = pf.image2array(fn, gl=True), None
    print(f"{color.shape=}")
    ny, nx = color.shape[:2]
    if colormap is not None:
        print("Size of colormap: %s" % str(colormap.shape))
    color = color.reshape(-1, 3)

    # Create a 2D grid of nx*ny elements
    F = pf.Formex('4:0123').replicm((nx, ny)).centered().translate(2, 1.)

    if part == "Plane":
        G = F
    else:
        if part == "Half Sphere":
            sx = 180.
        else:
            sx = 360.
        G = F.spherical(scale=[sx/nx, 180./ny, 2.*max(nx, ny)]).rollAxes(-1)
    pf.draw(G, color=color, colormap=colormap)
    pf.drawText('Created with pyFormex', (10, 10))


if __name__ == '__draw__':
    run()

# End
