#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Widgets


.. metadata
  :level: beginner
  :topics: dialog
  :techniques:
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    pf.error("This is a simulated error, to demonstrate how an error message "
             "would be shown to the user.\nJust click OK and the error will "
             "go away.")


    pf.warning("This is a warning. A warning draws attention of the user on "
               "special conditions.")

    pf.showInfo("""..

A text in ReST
==============

- The lowest level of message box is the *info* level.
  It just displays information for the user.
- ReST text is automatically detected if it starts with '..'.

""")

    pf.showInfo("""
<h1>A text in html.</h1>
Indeed, pf.showInfo can display plain text, ReST, markdown and html.<br/>
This should therefore start on a new line.
""")

    pf.ask("Answer this question with yes or no", ['Yes', 'No'])


    pf.selectItems(["a", "ccc", "bb", "dddd"], sort=True)


if __name__ == '__draw__':
    run()

# End
