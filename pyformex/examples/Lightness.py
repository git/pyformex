#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Lightness

This examples shows how to use an image file to set colors on a
structure.

The user can select an image file. A rectangular grid with corrersponding
number of cells is constructed. The grid is shown with the colors from the
image and can possibly be transformed to a sphere or a half sphere.

.. note::
  This example is slow when using high resolution images.

.. metadata
  :level: normal
  :topics: image
  :techniques: color, filename
"""
import numpy as np
import pyformex as pf
from pyformex import _I
from pyformex.color import lightness, luminance
_name = pf.Path(__file__).stem


channels = ['red', 'green', 'blue',
            'lightness', 'gb', 'rb', 'rg']
ncol = 4

def run():
    pf.resetAll()
    pf.flat()
    pf.clear()
    # Set default image filename
    filename = pf.cfg['datadir'] / 'butterfly.png'
    # Give user a change to change it
    viewer = pf.widgets.ImageView(filename, maxheight=200)
    res = pf.askItems(store=_name+'_data', items=[
        _I('filename', filename, text='Image file', itemtype='filename',
           filter='img', mode='exist', preview=viewer),
        viewer,
    ], modal=True)
    if not res:
        return

    filename = res['filename']
    color = pf.image2array(filename, gl=True)
    print(f"{color.shape=}")
    ny, nx = color.shape[:2]   # pixels move fastest in x-direction!
    color = color.reshape(-1, 3)
    F = pf.Formex('1:0').replicm((nx, ny))
    FA = pf.draw(F, color=color, nolight=True)

    def channel(ch):
        color = np.zeros((ny, nx, 3), dtype=np.float32)
        if ch == 'lightness':
            data = lightness(luminance(FA.color)).reshape(ny, nx, 1)
            color[..., [0, 1, 2]] = data
        elif ch == 'luminance':
            data = luminance(FA.color).reshape(ny, nx, 1)
            color[..., [0, 1, 2]] = data
        if ch in ('red', 'rg', 'rb'):
            color[..., 0] = FA.color[..., 0].reshape(ny, nx)
        if ch in ('green', 'rg', 'gb'):
            color[..., 1] = FA.color[..., 1].reshape(ny, nx)
        if ch in ('blue', 'rb', 'gb'):
            color[..., 2] = FA.color[..., 2].reshape(ny, nx)
        if ch == 'cyan':
            color[..., [1, 2]] = FA.color[..., 0].reshape(ny, nx, 1)
        if ch == 'magenta':
            color[..., [0, 2]] = FA.color[..., 1].reshape(ny, nx, 1)
        if ch == 'yellow':
            color[..., [0, 1]] = FA.color[..., 2].reshape(ny, nx, 1)
        return color.reshape(-1, 3)

    def trl(i):
        iy, ix = divmod(i, ncol)
        return np.array((ix, -iy, 0)) * F.sizes()

    for i, ch in enumerate(channels):
        t = trl(i+1)
        color=channel(ch)
        pf.draw(F.trl(t), color=channel(ch), nolight=True)


if __name__ == '__draw__':
    run()
# End
