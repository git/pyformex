#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""IsoContour

This example illustrates how to create isocontours through pixel data.

.. metadata
  :level: expert
  :topics: image, curve
  :techniques: isoline
"""
import numpy as np
import pyformex as pf
from pyformex import _I
from pyformex.color import lightness, luminance
from pyformex.plugins.isosurface import isoline
_name = pf.Path(__file__).stem


def show_contours(filename, value, n, levelmin, levelmax, npalette, alpha):
    color = pf.image2array(filename, gl=True)
    # print(f"{color.shape=}")
    ny, nx = color.shape[:2]   # pixels move fastest in x-direction!
    color = color.reshape(-1, 3)
    F = pf.Formex('1:0').replicm((nx, ny))
    FA = pf.draw(F, color=color, nolight=True)
    FA.alpha = 0.5

    if value == 'lightness':
        data = lightness(luminance(FA.color))
    elif value == 'luminance':
        data = luminance(FA.color)
    elif value == 'red':
        data = FA.color[..., 0]
    elif value == 'green':
        data = FA.color[..., 1]
    elif value == 'blue':
        data = FA.color[..., 2]
    data = data.reshape(ny, nx)

    # change levels to adjust number and position of contours
    levels = levelmin + np.arange(1, n) * (levelmax-levelmin) / n
    # print(levels)
    pf.canvas.settings.colormap = pf.refcfg['canvas/colormap'][:npalette]
    pf.transparent()
    for col, level in enumerate(levels):
        seg = isoline(data, level, nproc=-1)
        C = pf.Formex(seg)
        pf.draw(C, color=col, linewidth=3)

    if alpha == 0.0:
        pf.undraw(FA)
    else:
        FA.alpha = alpha


def run():
    pf.resetAll()
    pf.clear()
    # Set default image filename
    filename = pf.cfg['datadir'] / 'butterfly.png'
    # Give user a change to change it
    viewer = pf.widgets.ImageView(filename, maxheight=200)
    res = pf.askItems(store=_name+'_data', items=[
        _I('filename', filename, text='Image file', itemtype='filename',
           filter='img', mode='exist', preview=viewer),
        viewer,
        _I('value', None,
           choices=['lightness', 'luminance', 'red', 'green', 'blue']),
        _I('n', 10, text='Number of isocontours'),
        _I('levelmin', 0.0, text='Minimum level value'),
        _I('levelmax', 1.0, text='Maximum level value'),
        _I('npalette', 8, text='Number of different colors'),
        _I('alpha', 0.5, text='Opacity of image (0.0 is invisible)'),
    ])

    if res:
        show_contours(**res)


if __name__ == '__draw__':
    run()

# End
