#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Pattern

This example shows the predefined geometries from pf.simple.shape.

.. metadata
  :level: beginner
  :topics: geometry
  :techniques: color, pattern
"""
import pyformex as pf
from pyformex.opengl.decors import Grid
_name = pf.Path(__file__).stem


def run():
    pf.reset()
    pf.setDrawOptions(dict(view='front', linewidth=5, fgcolor='red'))
    grid = Grid(nx=(4, 4, 0), ox=(-2.0, -2.0, 0.0), dx=(1.0, 1.0, 1.0),
                planes='n', linewidth=1)
    pf.draw(grid)
    pf.linewidth(3)
    FA = None
    pf.setDrawOptions({'bbox': None})
    for n in pf.simple.Pattern:
        print("%s = %s" % (n, pf.simple.Pattern[n]))
        FA = pf.draw(pf.simple.shape(n), bbox=None, color='red', undraw=FA)
        pf.pause()


if __name__ == '__draw__':
    run()

# End
