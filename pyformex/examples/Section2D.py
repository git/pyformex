#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
#
"""Section2D

Computing geometrical properties of plane sections.

.. metadata
  :level: normal
  :topics: geometry, section2d
  :techniques:
"""
import pyformex as pf
from pyformex import _I
from pyformex.plugins import sectionize
from pyformex.plugins.section2d import sectionChar, extendedSectionChar
from pyformex.mydict import CDict
_name = pf.Path(__file__).stem


def showaxes(C, angle, size, color):
    H = pf.simple.shape('plus').scale(0.6*size).rot(angle/pf.DEG).trl(C)
    pf.draw(H, color=color)


def square_example(scale=[1., 1., 1.]):
    P = pf.Formex([[[1, 1]]]).rosette(4, 90).scale(scale)
    return sectionize.connectPoints(P, close=True)

def rectangle_example():
    return square_example(scale=[2., 1., 1.])

def circle_example():
    return pf.simple.circle(5., 5.)

def close_loop_example():
    # one more example, originally not a closed loop curve
    F = pf.Formex('l:11').replic(2, 1, 1) + pf.Formex('l:2').replic(2, 2, 0)
    M = F.toMesh()
    pf.draw(M, color='green')
    pf.drawNumbers(M, color='red')
    pf.drawNumbers(M.coords, color='blue')

    print("Original elements:", M.elems)
    conn = M.elems.chained()
    if len(conn) > 1:
        print("This curve is not a closed circumference")
        return None

    sorted = conn[0]
    print("Sorted elements:", sorted)

    pf.showInfo('Click to continue')
    pf.clear()
    M = pf.Mesh(M.coords, sorted)
    pf.drawNumbers(M)
    return M.toFormex()

def run():
    pf.clear()
    pf.flat()
    pf.reset()
    examples = {'Square': square_example,
                'Rectangle': rectangle_example,
                'Circle': circle_example,
                'CloseLoop': close_loop_example,
                }

    res = pf.askItems([
        _I('example', text='Select an example', choices=list(examples.keys())),
    ])
    if res:
        F = examples[res['example']]()
        if F is None:
            return
        pf.draw(F)
        S = sectionChar(F)
        S.update(extendedSectionChar(S))
        print(CDict(S))
        G = pf.Formex([[[S['xG'], S['yG']]]])
        pf.draw(G, bbox='last')
        showaxes([S['xG'], S['yG'], 0.], S['alpha'], F.dsize(), 'red')


if __name__ == '__draw__':
    run()

# End
