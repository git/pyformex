#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Sphere

.. metadata
  :level: normal
  :topics: geometry, surface, sphere
  :techniques: dialog, color
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    pf.clear()
    pf.wireframe()
    nx=32   # number of modules in circumferential direction
    ny=32   # number of modules in meridional direction
    rd=100  # radius of the sphere cap
    top=70  # latitude angle at the top (90 = closed)
    bot=-70  # latitude angle of the bottom (-90 = closed)

    a = ny*float(top)/(bot-top)

    # First, a line based model

    base = pf.Formex('l:543', [1, 2, 3])  # single cell
    pf.draw(base)

    d = base.select([0]).replicm((nx, ny))   # all diagonals
    m = base.select([1]).replicm((nx, ny))   # all meridionals
    h = base.select([2]).replicm((nx, ny))  # all horizontals
    f = m+d+h
    pf.draw(f)

    g = f.translate([0, a, 1]).spherical(scale=[360./nx, bot/(ny+a), rd])
    pf.clear()
    pf.draw(g)

    # Second, a surface model

    pf.clear()
    pf.flat()
    base = pf.Formex([[[0, 0, 0], [1, 0, 0], [1, 1, 0]],
                      [[1, 1, 0], [0, 1, 0], [0, 0, 0]]],
                     [1, 3])
    pf.draw(base)

    f = base.replicm((nx, ny))
    pf.draw(f)

    h = f.translate([0, a, 1]).spherical(scale=[360./nx, bot/(ny+a), rd])
    pf.clear()
    pf.draw(h)

    # Both

    g = g.translate([-rd, 0, 0])
    h = h.translate([rd, 0, 0])
    pf.clear()
    bb = pf.bbox([g, h])
    pf.draw(g, bbox=bb)
    pf.draw(h, bbox=bb)


if __name__ == '__draw__':
    run()

# End
