#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Curves

Examples showing the use of the 'curve' plugin

.. metadata
  :level: normal
  :topics: geometry, curve
  :techniques: widgets, persistence, import, spline, frenet
"""
import pyformex as pf
_name = pf.Path(__file__).stem

def run():
    pf.clear()
    X = pf.pattern('0214412214')
    print(X)

    def drawCurveWithPoints(C, color=0, linewidth=1):
        pf.draw(C, color=color, linewidth=linewidth)
        pf.draw(C.coords, color=color)
        pf.drawNumbers(C.coords, color=color)

    C = pf.BezierSpline(control=X)
    drawCurveWithPoints(C)

    if pf.ack('Split?'):
        CL = C.splitAt([0.3, 1.7, 2.5])
        for i, c in enumerate(CL):
            drawCurveWithPoints(c, color=i+1, linewidth=3)
            print("======= PART %s ========" % i)
            print(c.coords)
    else:
        D = C.insertPointsAt([0.3, 1.7, 2.5])
        drawCurveWithPoints(D, color='red', linewidth=3)
        print(D.coords)


if __name__ == '__draw__':
    run()
# End
