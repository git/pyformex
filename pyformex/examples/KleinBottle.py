#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Klein bottle

This example creates and displays a Quad4 Mesh model of a Klein bottle.
A Klein bottle is a non-orientable (single sided) surface without boundary.
The surface is self intersecting.

The geometry is created by computing the position of the nodes from
mathematical formulas.

.. metadata
  :level: beginner
  :topics: geometry, surface, mesh
  :techniques: dialog, bkcolor, transparency
"""
import pyformex as pf
from pyformex.gui.guicore import _I
from pyformex.candy import KleinBottle

_name = pf.Path(__file__).stem


def run():
    """Main script"""
    pf.clear()
    pf.smooth()
    pf.view('front')
    pf.transparent()

    res = pf.askItems(caption=_name, store=_name+'_data', items=[
        _I('nu', 128, text="#elements (longitudinal)"),
        _I('nv', 64, text="#elements (circumferential)"),
        _I('bottle', text="Geometry", itemtype='hradio',
           choices=['full bottle', 'half bottle']),
        _I('color', 1, min=0, max=15, text="Front color index"),
        _I('bkcolor', 3, min=0, max=15, text="Back color index"),
    ])

    if res:
        M = KleinBottle(
            nu=res['nu'], nv=res['nv'],
            rv=(0.0, 1.0) if res['bottle'].startswith('full') else (0.5, 1.0),
        )
        pf.draw(M, color=res['color'], bkcolor=res['bkcolor'])
        pf.PF[_name] = M


if __name__ == '__draw__':
    run()

# End
