#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Viewports.py

Demonstrate multiple viewports.

.. metadata
  :level: advanced
  :topics: surface
  :techniques: viewport, color
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def atExit():
    print("EXITING")
    pf.layout(1)
    pf.reset()

def run():
    pf.reset()
    pf.smoothwire()

    F = pf.Formex.read(pf.cfg['datadir'] / 'horse.pgf')

    pf.layout(1)
    pf.draw(F, view='front')
    pf.drawText('Viewport 0', (20, 20), size=20)

    pf.pause(msg='NEXT: Create Empty Viewport 1')
    pf.layout(2)
    pf.smooth()
    pf.draw(F, view='front')
    pf.drawText('Viewport 1', (20, 20), size=20)
    pf.GUI.viewports.updateAll()
    pf.pause()
    # return

    pf.pause(msg='NEXT: Create Viewport 2 and draw in green')
    pf.layout(3)
    pf.draw(F, color='green')

    pf.pause(msg='NEXT: Link Viewport 2 to Viewport 0')

    pf.linkViewport(2, 0)
    pf.GUI.viewports.updateAll()

    pf.pause(msg='NEXT: Create 4 Viewports all linked to Viewport 0')

    pf.layout(4, 2)
    pf.viewport(0)
    for i in range(1, 4):
        pf.linkViewport(i, 0)

    pf.pause(msg='NEXT: Change background colors in the viewports')

    colors=['indianred', 'olive', 'coral', 'yellow']

    for i, v in enumerate(['front', 'right', 'top', 'iso']):
        pf.viewport(i)
        pf.view(v)
        pf.bgcolor(colors[i])

    pf.pause(msg='NEXT: Cut the horse in viewport 3, notice results visible in all')

    pf.viewport(3)
    G = F.cutWithPlane([0., 0., 0.], [-1., 0., 0.], side='+')
    pf.clear()
    pf.draw(G)  # this draws in the 4 viewports !
    pf.GUI.viewports.updateAll()

    pf.pause(msg='NEXT: DONE')


if __name__ == '__draw__':
    run()

# End
