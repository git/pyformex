#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ContourView

Saves the contour of an object as shown in the viewport.

.. metadata
  :level: normal
  :topics: geometry, rendering
  :techniques: outline, contour
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def outlineBox(F, axis, dmin, dmax):
    """Construct an outlinebox from an outline curve

    F is a plex-2 Formex obtained from Canvas.outline()

    Returns a plex-4 Formex obtained by connecting two translated
    instance of the outline: one to the minimal z-value, one to the
    maximal z-value.
    """
    G1 = F.trl(axis, dmin)
    G2 = F.trl(axis, dmax)
    pf.draw([G1, G2], color='green')
    H = pf.formex.connect([G1, G1, G2, G2], nodid=[0, 1, 1, 0])
    pf.draw(H, color='orange')


def run():
    pf.resetAll()
    pf.smooth()
    pf.clear()
    S = pf.candy.Horse()
    pf.perspective(False)
    pf.draw(S, color='black')   # Default fgcolor (darkgrey) doesn't work
    pf.setDrawOptions(view='cur', bbox=None)
    pf.pause()

    G = pf.canvas.outline(size=[-1, 2000])
    pf.draw(G, color='red', ontop=True)
    pf.pause()

    axis = G.attrib.axis
    dmin, dmax = S.directionalSize(axis)
    outlineBox(G, axis, dmin, dmax)
    pf.perspective()


if __name__ == '__draw__':
    run()
    pf.perspective(True)

# End
