#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""ExtrudeBorder

This example illustrates some surface techniques. A closed surface is cut
with a number(3) of planes. Each cut leads to a hole, the border of which is
then extruded over a gioven length in the direction of the plane's positive
normal.

.. metadata
  :level: normal
  :topics: surface
  :techniques: extrude, borderfill, cut
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def cutBorderClose(S, P, N):
    """Cut a surface with a plane, and close it

    Return the border line and the closed surface.
    """
    S = S.cutWithPlane(P, N, side='-')
    B = S.borderMeshes()[0]
    return B, S.close()


def run():
    pf.smooth()
    pf.linewidth(2)
    pf.clear()
    S = pf.simple.sphere()
    SA = pf.draw(S)

    p = 0
    for P, N, L, div in (
        # Each line contains a point, a normal, an extrusion length
        # and the number of elements along this length
        ((0.6, 0., 0.), (1., 0., 0.), 2.5, 5),
        ((-0.6, 0.6, 0.), (-1., 1., 0.), 4., (16, 1.0, 1.0)),
        ((-0.6, -0.6, 0.), (-1., -1., 0.), 3., 2),
    ):
        B, S = cutBorderClose(S, P, N)
        pf.draw(B)
        p += 1
        E = B.extrude(div, dir=pf.at.normalize(N), length=L, eltype='tri3').setProp(p)
        pf.draw(E)

    pf.draw(S)
    pf.undraw(SA)
    pf.zoomAll()


if __name__ == '__draw__':
    run()
# End
