#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Sweep

This example illustrates the creation of spiral curves and the sweeping
of a plane cross section along thay curve.

.. metadata
  :level: normal
  :topics: geometry, curve, mesh
  :techniques: sweep, spiral
"""
import re
import random
import numpy as np
import pyformex as pf
from pyformex import _I, _G
_name = pf.Path(__file__).stem

# Speed functions of the spiral
rfuncs = {
    'constant': lambda x, a: a[0],
    'linear (Archimedes)': lambda x, a: a[0] + a[1]*x,
    'quadratic': lambda x, a: a[0] + a[1]*x + a[2]*x*x,
    'exponential (equi-angular)': lambda x, a: a[0] + a[1] * np.exp(a[2]*x),
}
rfunc_choices = list(rfuncs)
rfunc_default = rfunc_choices[1]

# Planar cross sections
cross_sections = {}
# planar patterns from pf.simple
for cs in pf.simple.Pattern:
    if re.search('[a-zA-Z]', pf.simple.Pattern[cs][2:]) is None:
        cross_sections[cs] = pf.simple.Pattern[cs]
# add some more patterns
cross_sections.update({
    'channel': 'l:1223',
    'H-beam': 'l:11/322/311',
    'sigma': 'l:16253',
    'Z-beam': 'l:353',
    'octagon': 'l:15263748',
    'angle_cross': 'l:12+23+34+41',
    'triangle_cross': '3:012023034041',
    'solid_square': '4:0123',
    'solid_triangle': '3:012',
})
cross_choices = list(cross_sections.keys())
random.shuffle(cross_choices)

dialog_items = [
    _I('nseg', 100, text='Number of cells along spiral'),
    _I('turns', 2.5, text='Number of 360 degree turns'),
    _I('rfunc', rfunc_default, text='Spiral function', choices=rfunc_choices),
    _I('coeffs', (1., 0.5, 0.2), text='Coefficients in the spiral function'),
    _I('spiral3d', 0.0, text='Out of plane factor'),
    _I('spread', False, text='Spread points evenly along spiral'),
    _I('nwires', 1, text='Number of spirals'),
    _G('sweep', text='Sweep Data', check=False, items=[
        _I('cross_section', choices=cross_choices,
            text='Shape of cross section'),
        _I('cross_rotate', 0.,
            text='Cross section rotation angle before sweeping'),
        _I('cross_upvector', '2',
            text='Cross section vector that keeps its orientation'),
        _I('cross_scale', 0., text='Cross section scaling factor'),
    ]),
    _I('flyalong', False, text='Fly along the spiral'),
]

def drawSpiralCurve(PL, nwires, color1, color2=None):
    if color2 is None:
        color2 = color1
    # Convert to Formex, because that has a rosette() method
    PL = PL.toFormex()
    if nwires > 1:
        PL = PL.rosette(nwires, 360./nwires)
    pf.draw(PL, color=color1)
    pf.draw(PL.coords, color=color2, marksize=5, ontop=True)


def createCrossSection(section, rotate, scale):
    CS = pf.Formex(cross_sections[section])
    if rotate:
        CS = CS.rotate(rotate)
    if scale:
        CS = CS.scale(scale)
    CS = CS.swapAxes(0, 2)
    # Convert to Mesh, because that has a sweep() method
    return CS.toMesh()


def createSpiralCurve(turns, nseg, rfunc, coeffs, spiral3d, **kargs):
    from functools import partial
    rf = partial(rfuncs[rfunc], a=coeffs)
    if spiral3d:
        zf = lambda x: spiral3d * rf(x)
    else:
        zf = lambda x: 0.0
    X = pf.simple.grid1(nseg+1).scale(turns*2*np.pi/nseg)
    Y = X.spiral((1, 0, 2), rfunc=rf, zfunc=zf, angle_spec=pf.RAD)
    return pf.PolyLine(Y)


def show():
    """Accept the data and draw according to them"""
    if not dialog.validate():
        print("INVALID DATA")
        return
    pf.clear()
    res = dialog.results
    print(res)
    spread = res.pop('spread')
    sweep = res.pop('sweep')
    nwires = res.pop('nwires')
    flyalong = res.pop('flyalong')
    upvector = res.pop('cross_upvector')
    cross = pf.utils.subDict(res, prefix='cross_', strip=True, remove=True)
    print("SPIRAL: ", res)
    PL = createSpiralCurve(**res)
    print(PL)
    drawSpiralCurve(PL, nwires, 'red', 'blue')

    if spread:
        at = PL.atLength(PL.nparts)
        X = PL.pointsAt(at)
        PL = pf.PolyLine(X)
        pf.clear()
        drawSpiralCurve(PL, nwires, 'green', 'blue')

    if sweep:
        CS = createCrossSection(**cross)
        pf.clear()
        pf.draw(CS)
        pf.wait()
        structure = CS.sweep(PL, normal=[1., 0., 0.],
                             upvector=eval(upvector), avgdir=True)
        pf.clear()
        pf.smoothwire()
        pf.draw(structure, color='red', bkcolor='cyan')

        if nwires > 1:
            structure = structure.toFormex().rosette(nwires, 360./nwires).toMesh()
            pf.draw(structure, color='orange')

    if flyalong:
        pf.flyAlong(PL.scale(1.1).trl([0.0, 0.0, 0.2]), upvector=[0., 0., 1.],
                    sleeptime=0.1)

        pf.view('front')


def close():
    global dialog
    pf.PF['Sweep_data'] = dialog.results
    if dialog:
        dialog.close()
        dialog = None
    pf.script.scriptRelease(__file__)


def timeOut():
    """What to do on a Dialog timeout event.

    As a policy, all pyFormex examples should behave well on a
    dialog timeout. This is important for developers.
    Most normal users can simply ignore it.
    """
    show()
    close()


def createDialog():
    global dialog
    # Create the dialog
    dialog = pf.Dialog(
        caption=_name, store=_name+'_ data', items=dialog_items,
        actions=[('Close', close), ('Show', show)], default='Show')


def run():
    # Show the dialog and let the user have fun
    pf.linewidth(2)
    pf.clear()
    createDialog()
    dialog.show(timeoutfunc=timeOut)
    pf.script.scriptLock(__file__)


if __name__ == '__draw__':
    run()

# End
