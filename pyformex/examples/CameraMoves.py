#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Camera Moves

This illustrates some of the functionality for moving the camera.

It displays a Baumkuchen structure (see the Baumkuchen example)
but the camera only shows the left part of it. Then the user is asked how
to move the camera along the rest of the structure and revert to the
initial display. Three different methods are offered:

- Pan: the camera is rotated along its vertical axis
- Truck: the camera is moved parallel to the structure
- Digital: the camera image is translated over the viewport

Notice how the first two methods change the perspective, while the last one
doesn't.

.. metadata
  :level: beginner
  :topics: camera
  :techniques: color, bump
"""
import pyformex as pf
_name = pf.Path(__file__).stem

from pyformex.examples.Baumkuchen import Baumkuchen  # noqa: E402

def do_step(func, nsteps, inc):
    for i in range(nsteps):
        pf.sleep(2 / nsteps)
        func(inc)
        pf.canvas.update()

def do_2step(func, nsteps, inc):
    do_step(func, nsteps, inc)
    do_step(func, nsteps, -inc)

def do_4step(func, nsteps, inc):
    do_step(func, nsteps, inc)
    do_step(func, 2*nsteps, -inc)
    do_step(func, nsteps, inc)

def run():
    pf.frontview('xz')
    pf.view('xz')
    pf.clear()
    pf.smoothwire()
    B = Baumkuchen(m=12, n=36, k=12, e1=30, e2=5)
    # display 1/4 of the structure
    bb = B.bbox()
    bb[1][0] = 0.75*bb[0][0] + 0.25*bb[1][0]
    pf.draw(B, color='blue', bbox=bb)
    cam = pf.canvas.camera
    options = ['Truck', 'Pan', 'Digital', 'Cancel']
    default = 0
    nsteps = 200
    while True:
        method = pf.ask("Pan method?", options, default=options[default])
        default += 1
        if method == 'Digital':
            pf.printc("Digital: move the camera image over the viewport",
                      color='darkgreen')
            func = pf.panLeft
            inc = 1 / nsteps
        elif method == 'Truck':
            pf.printc("Truck: move the camera along the x-axis",
                      color='darkcyan')
            func = cam.truck
            inc = B.sizes()[0] / nsteps
        elif method == 'Pan':
            pf.printc("Pan: rotate the camera around its vertical axis",
                      color='darkmagenta')
            func = cam.pan
            inc = 45. / nsteps
        else:
            break
        do_2step(func, nsteps, inc)
        pf.script.breakpt("CameraMoves breakpoint")


if __name__ == '__draw__':
    run()
# End
