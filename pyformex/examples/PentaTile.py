#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""PentaTile

This example shows the tiling of a plane with different kind of pentagons.

See also: https://en.wikipedia.org/wiki/Pentagonal_tiling

.. metadata
  :level: advanced
  :topics: geometry
  :techniques: formex
"""
import numpy as np
import pyformex as pf
_name = pf.Path(__file__).stem


def reflect_line(F, x1, x2):
    """Reflect a polygon along the line x1-x2"""
    norm = (x1 - x2).rotate(90.)
    return F.reflect(norm, x1)

def replicate(F, trl, n):
    return pf.List(F.trl(trl*i) for i in range(n))

def pentagonMann():
    """Creates a single Mann pentagon

    See http://www.huffingtonpost.com/2015/08/18/historic-tile-discovery-gives-math-world-a-big-jolt_n_8010250.html?utm_hp_ref=world&ir=World   # noqa: 501
    """
    # lengths of the sides
    lengths = np.array([1., 0.5, 1./np.sqrt(2.)/(np.sqrt(3.)-1), 0.5, 0.5])
    # inner angles of the pentagons
    angles = np.array([60., 135., 105., 90., 150.,])
    rotations = 180.-angles
    grotations = np.roll(rotations.cumsum(), 1)
    print("lengths: %s" % lengths)
    print("angles: %s" % angles)
    print("rotations: %s" % rotations)
    print("cumulative rotations: %s" % grotations)
    u = pf.Coords([1., 0., 0.])
    X = pf.Coords([u.scale(l).rotate(r) for l, r in zip(lengths, grotations)])
    Y = X.cumsum(axis=0)
    return pf.Polygons(Y, [range(5)])

def tileMann(nrep, nrep1, altcolors=False, delay=1):
    pf.delay(delay)
    F = pentagonMann().setProp(1)
    pf.printc("1. Cyan: A single Mann pentagon")
    pf.draw(F)
    # pf.drawNumbers(F.coords)
    # pf.drawMarks([F.center()], ['F'])
    G = reflect_line(F, F.coords[4], F.coords[0]).setProp(2)
    pf.printc("2. Orange: Reflected about the bottom side")
    pf.draw(G)
    # pf.drawNumbers(G.coords)
    # pf.drawMarks([G.center()], ['G'])
    H = reflect_line(F, F.coords[1], F.coords[2]).setProp(3 if altcolors else 2)
    pf.printc("3. Yellow: Reflected about the top side")
    pf.draw(H)
    # pf.drawNumbers(H.coords)
    # pf.drawMarks([H.center()], ['H'])
    trl = F.coords[1] - G.coords[3]
    FGH = F+G+H
    # pf.draw(FGH)
    FGHr = FGH.rotate(180, around=0.5*(G.coords[1]+G.coords[2]))
    # FGHr.prop[FGHr.prop==2] = 4
    pf.printc("4. Rotate 180 degree around center of bottom side and add")
    pf.draw(FGHr)
    cell = FGH+FGHr
    pf.draw(cell)
    # pf.drawNumbers(cell.coords)
    cell1 = cell.reflect(0).trl([3.0, 0., 0.])
    if not altcolors:
        cell1.setProp(3 - cell.prop)
    print(cell.prop)
    print(cell1.prop)
    cell1 = cell1.position(cell1.coords[[14, 13, 10]], cell.coords[[17, 18, 28]])
    pf.printc("5. Add mirrored cells aligned at the bottom")
    pf.draw(cell1)
    # pf.drawNumbers(cell1.coords, color='red')
    trl1 = cell1.coords[27] - cell.coords[14]
    tiling = replicate(cell+cell1, trl, nrep)
    pf.PF['tiling'] = tiling
    pf.printc(f"6 Replicate {nrep} times in horizontal direction")
    pf.draw(tiling, clear=True)
    tiling1 = replicate(tiling, trl1, nrep1)
    pf.printc(f"7. Replicate {nrep1} times in vertical direction")
    pf.draw(tiling1, clear=True)


def reset_canvas():
    pf.reset()
    pf.flat()
    pf.wireMode(1)
    pf.clear()
    pf.canvas.settings.colormap = pf.colorArray(
        ['black', 'cyan', 'orange', 'yellow', 'darkcyan'])


def run():
    pf.resetAll()
    reset_canvas()
    tileMann(10, 3, altcolors=False)


if __name__ == '__draw__':
    run()

# End
