#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Clock

Shows an analog clock running for a short time

.. metadata
  :level: advanced
  :topics:
  :techniques: timer
"""
from datetime import datetime
import pyformex as pf
from pyformex import _I
from pyformex.opengl.textext import FontTexture
_name = pf.Path(__file__).stem


class AnalogClock():
    """An analog clock built from Formices"""

    def __init__(self, lw=2, mm=0.75, hm=0.85, mh=0.7, hh=0.6, sh=0.9):
        """Create an analog clock."""
        self.linewidth = lw
        self.circle = pf.simple.circle()
        radius = pf.Formex('l:2')
        self.mainmark = radius.subdivide([mm, 1.0])
        self.hourmark = radius.subdivide([hm, 1.0])
        self.mainhand = radius.subdivide([0.0, mh])
        self.hourhand = radius.subdivide([0.0, hh])
        if sh > 0.0:
            self.secshand = radius.subdivide([0.0, sh])
        else:
            self.secshand = None
        self.hands = []
        self.timer = None
        self.actor = None

    def draw(self):
        """Draw the clock (without hands)"""
        pf.draw(self.circle, color='black', linewidth=self.linewidth)
        pf.draw(self.mainmark.rosette(4, 90), color='black',
                linewidth=self.linewidth)
        pf.draw(self.hourmark.rot(30).rosette(2, 30).rosette(4, 90),
                color='black', linewidth=0.5*self.linewidth)

    def drawTime(self, hrs, min, sec=None):
        """Draw the clock's hands showing the specified time.

        If no seconds are specified, no seconds hand is drawn.
        """
        hrot = - hrs*30. - min*0.5
        mrot = - min*6.
        MH = pf.draw(self.mainhand.rot(mrot), bbox=None, color='red',
                     linewidth=self.linewidth)
        HH = pf.draw(self.hourhand.rot(hrot), bbox=None, color='red',
                     linewidth=self.linewidth)
        self.hands = [MH, HH]
        if self.secshand and sec:
            srot = - sec*6.
            SH = pf.draw(self.secshand.rot(srot), bbox=None, color='orange',
                         linewidth=0.5*self.linewidth)
            self.hands.append(SH)
        pf.undraw(self.actor)
        self.actor = self.hands

    def drawNow(self):
        """Draw the hands showing the current time."""
        now = datetime.now()
        self.drawTime(now.hour, now.minute, now.second)
        pf.script.breakpt("The clock has been stopped!")

    def run(self, granularity=1, runtime=100):
        """Run the clock for runtime seconds, updating every granularity."""
        if granularity > 0.0:
            self.timer = pf.Timer(granularity, self.drawNow, repeat=True)
        if runtime > 0.0:
            self.timeout = pf.Timer(runtime, self.stop)

    def stop(self):
        """Stop a running clock."""
        print("STOP")
        if self.timer:
            self.timer.stop()


class DigitalClock():
    """A digital clock"""

    def __init__(self, seconds=True):
        """Create an analog clock."""
        self.seconds = bool(seconds)
        self.len = 8 if self.seconds else 5
        self.F = pf.Formex('4:0123').replic(self.len)
        self.ft = FontTexture('NotoSansMono-Condensed.3x32.png', 24)
        self.timer = None
        self.actor = None

    def drawTime(self, hrs, min, sec):
        """Draw the clock with the specified time"""
        text = f"{hrs:02d}:{min:02d}"
        if self.seconds:
            text += f":{sec:02d}"
        tc = self.ft.texCoords(text)
        A = pf.draw(self.F, color='pyformex_pink', texture=self.ft, texcoords=tc,
                    texmode=2, ontop=True)
        pf.undraw(self.actor)
        self.actor = A

    def drawNow(self):
        """Draw the hands showing the current time."""
        now = datetime.now()
        self.drawTime(now.hour, now.minute, now.second)
        pf.script.breakpt("The clock has been stopped!")

    draw = drawNow

    def run(self, granularity=1, runtime=100):
        """Run the clock for runtime seconds, updating every granularity."""
        if granularity > 0.0:
            self.timer = pf.Timer(granularity, self.drawNow, repeat=True)
        if runtime > 0.0:
            self.timeout = pf.Timer(runtime, self.stop)

    def stop(self):
        """Stop a running clock."""
        print("STOP")
        if self.timer:
            self.timer.stop()


def run():
    pf.reset()
    res = pf.askItems(items=[
        _I('Clock type', choices=['Analog', 'Digital'], itemtype='radio'),
        _I('runtime', 15, text='Run time (seconds)'),
    ])
    if res:
        if res['Clock type'] == 'Analog':
            C = AnalogClock()
        else:
            C = DigitalClock()
        C.draw()
        pf.setDrawOptions({'bbox': None, 'view': None})
        C.drawNow()
        C.run()
        pf.sleep(res['runtime'])
        C.stop()


if __name__ == '__draw__':
    run()

# End
