#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Casteljau

This example illustrates the de Casteljau algorithm for constructing a point
on a  Bezier curve.

User input:

:pattern: a string defining a set of points (see 'pattern' function).
  It can be selected from a number of predefined values, or be set as
  a custom value.
:custom: a custom string to be used instead of one of the predefined values.
:value: a parametric value between 0.0 and 1.0.

The pattern string defines a set of N points, where N is the length of the
string. These N points define a Bezier Spline of degree N-1.

The application first draws a PolyLine through the N points. Then it draws
the subsequent steps of de Casteljau's algorithm to construct the point of
the Bezier Spline for the given parametric value.

Finally it also draws a whole set of points on the Bezier Spline. These points
are computed from Bernstein polynomials, just like in example BezierCurve.

.. metadata
  :level: normal
  :topics: geometry, curve
  :techniques: pattern, delay
"""
import numpy as np
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


# Some strings defining line patterns
predefined = [
    '02584',
    '058',
    '0214',
    '0184',
    '0514',
    '01234',
    '051414336',
    '05858585858',
    '012345678',
    'custom']

# Default values
pattern = None    # The chosen pattern
custom = ''       # The custom pattern
value = 0.5   # Parametric value of the point to construct


def show_decasteljau(x, t):
    """Show the deCasteljau construction of a point on a

    Parameters
    ----------
    x: Coords
        The coordinates of the control points of the curve
    t: float
        The parametric value for which to construct a point
    """
    # Construct and show a polyline through the points x
    pf.draw(x)
    pf.drawNumbers(x)
    pf.draw(pf.PolyLine(x), bbox='auto', view='front')
    pf.setDrawOptions({'bbox': None})
    # Compute and show the deCasteljau construction
    Q = pf.curve.deCasteljau(x, t)
    pf.delay(0.8)
    pf.wait()
    for q in Q[1:-1]:
        pf.draw(pf.PolyLine(q), color='red')
    pf.draw(Q[-1], marksize=10)
    # Compute and show many points on the Bezier
    pf.delay(0)
    n = 100
    u = np.arange(n+1)*1.0/n
    P = pf.nurbs.pointsOnBezierCurve(x, u)
    pf.draw(pf.Coords(P))


def run():
    pf.flat()
    pf.clear()
    pf.linewidth(2)
    res = pf.askItems(store=_name+'_data', items=[
        _I('pattern', choices=predefined),  # Selected pattern
        _I('custom', ''),  # Custom pattern
        _I('value', 0.5),  # Parametric value of the point
    ], enablers=[('pattern', 'custom', 'custom')])

    if not res:
        return

    pattern = res['pattern']
    if pattern == 'custom':
        pat = res['custom']
    else:
        pat = pattern
    x = pf.Coords(pat)
    show_decasteljau(x, res['value'])


if __name__ == '__draw__':
    run()
# End
