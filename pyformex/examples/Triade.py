#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""SetTriade.py

This example shows how to use a customized Triade

.. metadata
  :level: advanced
  :topics: decors
  :techniques: setTriade
"""
import pyformex as pf
_name = pf.Path(__file__).stem


def run():
    global _triade_geometry

    pf.clear()
    pf.resetAll()
    pf.smoothwire()

    print('Draw something')
    F = pf.Formex.read(pf.cfg['datadir'] / 'horse.pgf').scale(10)
    pf.draw(F)
    pf.pause()

    print('Set default Triade on')
    pf.setTriade(on=True)
    pf.pause()

    print('Set Triade off')
    pf.setTriade(False)
    pf.pause()

    print('Set a man as triade, right bottom')
    pf.setTriade(triade='man', pos='rb')
    pf.pause()

    print('Set a cube as triade, left top')
    cube = pf.simple.shape('cube').setProp(
        [1, 2, 1, 2, 3, 3, 3, 3, 1, 2, 1, 2]).centered()
    pf.setTriade(on=True, pos='lt', triade=cube)
    pf.pause()

    print('Use the displayed geometry (F) itself as Triade, left center')
    pf.setTriade(on=True, pos='lc', triade=F)
    pf.pause()

    print('Reset the original Triade, center top')
    pf.setTriade(on=True, pos='ct', triade='axes')


if __name__ == '__draw__':
    run()

# End
