#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""BezierSpline

This example shows a collection of Bezier Spline curves through a number of
points. The points used are the corners and midside points of a unit square.
The user is asked for a number of points to use.
The image shows open (left) and closed (right) BezierSpline curves of
degrees 1(red), 2(magenta) and 3(blue).

.. metadata
  :level: normal
  :topics: geometry, curve
  :techniques: spline, dialog
"""
import numpy as np
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem

# Predefined set of points
_pts = pf.Coords('123344112')
npts = 5  # actual number of points used

def run():
    global npts  # makes this value persistent
    pf.resetAll()
    pf.clear()
    pf.linewidth(2)

    # Ask the user how many points he wants to use
    res = pf.askItems([
        _I('npts', npts, text=f"How many points to use (2..{len(_pts)})")
    ])
    if not res:
        return

    # Keep only the requested number of points
    npts = res['npts']
    pts = _pts[:npts]

    # Show open and closed Bezier Splines, for degrees 1,2,3
    degrees = [1, 2, 3]
    colors = ['red', 'green', 'blue']
    collection = {}
    for closed in [False, True]:
        pf.draw(pts)
        pf.drawNumbers(pts)
        for d, c in zip(degrees, colors):
            print("Degree %s, %s" % (d, closed))
            B = pf.BezierSpline(pts, closed=closed, degree=d)
            collection["BezierSpline-degree:%s-closed:%s" % (d, closed)] = B
            pf.draw(B, color=c)

            t = np.arange(2*B.nparts)*0.5
            ipts = B.pointsAt(t)
            pf.draw(ipts, color=c, marksize=10)
            idir = B.directionsAt(t)
            pf.drawVectors(ipts, 0.2*idir)
        # Translate the points to the right
        pts = pts.trl(0, 2.5)  # [:-1]
    pf.zoomAll()
    pf.PF.update(collection)


if __name__ == '__draw__':
    run()
# End
