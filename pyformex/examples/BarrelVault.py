#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""Barrel Vault

Create a parametric barrel vault.

.. metadata
  :level: beginner
  :topics: frame
  :techniques: dialog
"""
import pyformex as pf
from pyformex import _I
_name = pf.Path(__file__).stem


def create_barrel_beams(m, n, r, a, l):
    """Construct a barrel vault with beam elements

    See source below for meaning of parameters
    """
    # Longitudinals
    h = pf.Formex('l:1', 3).replicm((2*m, 2*n+1))
    # End bars
    e = pf.Formex('l:2', 0).replicm((2, 2*n), (2*m, 1))
    # Diagonals
    d = pf.Formex('l:5', 1).rosette(4, 90).translate([1, 1, 0]).replicm((m, n), (2, 2))
    # Create barrel
    barrel = (d+h+e).rotate(90, 1).translate(0, r).scale(
        [1., a/(2*n), l/(2*m)]).cylindrical()
    return barrel


def run():
    global barrel
    pf.reset()
    pf.wireframe()

    res = pf.askItems([
        _I(name='m', value=10, text='number of modules in axial direction'),
        _I(name='n', value=8, text='number of modules in tangential direction'),
        _I(name='r', value=10., text='barrel radius'),
        _I(name='a', value=180., text='barrel opening angle'),
        _I(name='l', value=30., text='barrel length'),
    ])
    if res:
        barrel = create_barrel_beams(**res)
        pf.draw(barrel, linewidth=2)


if __name__ == '__draw__':
    run()
# End
