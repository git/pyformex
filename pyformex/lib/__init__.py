#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""pyFormex C library module initialisation.

This tries to load the compiled libraries, and replaces those that failed
to load with the (slower) Python versions, if possible.
"""
import importlib
import pyformex as pf

__all__ = ['misc', 'nurbs', 'clust', 'accelerated']

misc = nurbs = clust = None
accelerated = []

if pf.options.uselib:
    def import_accel(name):
        """Import an accelerated lib module by name"""
        try:
            module = importlib.import_module(f"pyformex.lib.{name}_c")
        except ImportError:
            if pf.debugon(pf.DEBUG.LIB):
                print(f"Error while loading pyFormex compiled library {name}_c")
            return
        # check the version of the imported module
        if pf.debugon(pf.DEBUG.LIB):
            print("Succesfully loaded pyFormex compiled module"
                  f" {module.__name__} (version {module.__version__})")
        if module.__version__ == pf.__version__:
            accelerated.append(module)
            return module
        else:
            raise RuntimeError(
                "Incorrect acceleration module version"
                f" (have {module.__version__}, required {pf.__version__}).\n"
                "If you are running pyFormex directly from sources,"
                " you should run 'make lib'"
                " in the top directory of your pyFormex source tree.\n"
                "Else, pyFormex was likely not installed correctly.")

    misc = import_accel('misc')
    nurbs = import_accel('nurbs')
    clust = import_accel('clust')

# Load Python libraries if acceleration libraries failed
def import_emul(name):
    """Import an emulated lib module by name"""
    if pf.debugon(pf.DEBUG.LIB):
        print("Using the (slower) Python{name} functions")
    return importlib.import_module(f"pyformex.lib.{name}_e")


if misc is None:
    misc = import_emul('misc')
if nurbs is None:
    nurbs = import_emul('nurbs')

if pf.debugon(pf.DEBUG.LIB | pf.DEBUG.INFO):
    print("Accelerated modules:")
    for m in accelerated:
        print(f"  {m}")
if pf.debugon(pf.DEBUG.LIB):
    print(misc, nurbs, clust)

# make sure we could at least import one version of misc
assert misc is not None

# End
