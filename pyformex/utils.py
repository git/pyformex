#
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
"""A collection of miscellaneous utility functions

"""

import os
import re
import io
import random
import warnings
import functools

import pyformex as pf
from pyformex.path import Path
from pyformex import process
from pyformex.software import Module

shuffle = random.shuffle


def pzf_register(clas):
    """Class decorator to allow load from PZF format.

    Adding this decoratot to a class registers the class with the
    :mod:`pzffile` module. Objects in a PZF file with class name equal
    to clas.__name__ will then be restored using this class.
    """
    from pyformex import pzffile
    pzffile.register(clas)
    return clas


### Decorators ###

def memoize(func):
    """Remember the result of an instance method call.

    This is a decorator function that saves the result of an instance
    method call into the instance.
    Subsequent use of the function will return the result from
    memory instead of recomputing it.

    Notes
    -----
    If the decorated function has no arguments other than self, this
    decorator can be stacked with @property to create a cached property.

    The result is saved in a dict with the function name and its arguments
    as key. The dict is stored as an instance attribute _memory. It is
    created automatically on first used of a memoizing method.

    Examples
    --------
    We create a class with a single method, returning a list of 10
    random ints in the range from 0 to 20. The method's result is
    memoized.

    >>> class C:
    ...     @memoize
    ...     def random_ints(self):
    ...         print("Computing random ints")
    ...         return [random.randint(0,20) for i in range(10)]

    We create an instance and call the random_ints method.

    >>> c = C()
    >>> a = c.random_ints()
    Computing random ints
    >>> print(len(a), min(a) >= 0, max(a) <= 20)
    10 True True

    When calling the random_ints method again, the method is not actually
    executed, but the memoized values are returned, so they are the same
    as the previous.

    >>> b = c.random_ints()
    >>> print(len(b), a == b)
    10 True

    If we create another instance, we get other values, because the
    memoizing is done per instance.

    >>> b = C().random_ints()
    Computing random ints
    >>> print(len(b), a == b)
    10 False

    The results are stored in the _memory attribute. They can be deleted
    to force recomputation.

    >>> print(c._memory)
    {'random_ints': [...]}
    """
    @functools.wraps(func)
    def wrapper(self, *args, **kargs):
        key = func.__name__
        if args:
            key += str(args)
        if kargs:
            key += str(kargs)
        if not hasattr(self, '_memory'):
            self._memory = {}
        if key in self._memory:
            # return saved result
            res = self._memory[key]
        else:
            # compute and memoize result
            self._memory[key] = res = func(self, *args, **kargs)
        return res
    return wrapper


######### WARNINGS ##############

class PyformexWarning(UserWarning):
    """Warning category for announcing changes in pyFormex code

    This is used to warn the user about changes and deprecations
    in pyFormex. The messages from this category are normally stored
    in the messages module. The user can filter the messages out for
    the current and future sessions.
    """
    name = 'pyFormex Warning'

class PyformexRuntimeWarning(UserWarning):
    """Warning category for run time warnings

    Run time warnings are warnings to the end user that can only be filtered
    out for the current session.
    """
    name = 'Runtime Warning'

class PyformexRuntimeError(UserWarning):
    """Warning category for run time errors

    Run time error are obvious errors that should always be notified to
    the user. They can not be filtered out.
    """
    name = 'Runtime Error'


def is_pyformex_warning(exc):
    """Check that an exception is one of the Pyformex Warnings"""
    return exc.__name__.startswith('Pyformex')


def rev_lookup(dictionary, value, default=None):
    """Reverse lookup in a dict

    Lookup a value in a dict, and return its key (the first match).

    Parameters
    ----------
    dictionary: dict
        The dict in which to lookup a value
    value: anything
        The value to lookup in the dict
    default: anything
        The key to return if the value was not found

    Returns
    -------
    key: anything
        The first key in the dict whose value matches the given *value*,
        or *default* if no match was found.
    """
    for key, val in dictionary.items():
        if val == value:
            return key
    return default


def warningCategory(category):
    """Return a warning category and its alias

    The input can be a category or an alias. Returns both as a tuple
    (alias, category).
    Invalid values return the default category ('W', Warning).
    """
    _warn_category = {
        'P': PyformexWarning,
        'PR': PyformexRuntimeWarning,
        'W': Warning,
        'U': UserWarning,
        'R': RuntimeWarning,
        'F': FutureWarning,
    }
    if category in _warn_category:
        alias = category
    else:
        alias = rev_lookup(_warn_category, category, 'W')
    return alias, _warn_category[alias]


def warningAction(action):
    """Return a warning action and its alias

    The input can be an action string or an alias. Returns both as a tuple
    (alias, action).
    Invalid values return the default action ('i', 'ignore').
    """
    _warn_action = {     # ordered from less to more verbosity
        'i': 'ignore',      # never
        'o': 'once',        # once per session
        'm': 'module',      # once for each module
        'd': 'default',     # once for each position
        'a': 'always',      # always
        'e': 'error',       # rais exception
    }
    if action in _warn_action:
        alias = action
    else:
        alias = rev_lookup(_warn_action, action, 'i')
    return alias, _warn_action[alias]


silent_chars = '()^$*+?{}[]|'
re_trans_table = str.maketrans(silent_chars, '.'*len(silent_chars))

def retrans(s):
    """Transform a string to an re to match itself

    Replaces all special characters.
    """
    return s.translate(re_trans_table)


def filter_warning(action, message, category, module, save=''):
    """Add a warnings filter.

    Parameters
    ----------
    action:
        The warning filter action or alias.
    message: str | Warning | None
        The text of the message to be filtered.
    category:
        The warning category or alias.
    module: str
        The module in which the warning is to be filtered. '' for all.
    save: str
        The action for future sessions. If not empty, the filter is saved
        in the user settings.
    """
    if isinstance(message, Warning):  # Important
        category = message.__class__
        message = str(message)
    message = retrans(message)
    c, category = warningCategory(category)
    module = module if module else ''  # Allows for module being None
    if save:
        # Add filter for future sessions
        pf.prefcfg['warnings/filters'].add((save, message, c, module))
        if pf.debugon(pf.DEBUG.WARNING):
            print("Add future warnings filter: "
                  f"({save!r}, {message!r}, {c!r}, {module!r})")
            print("Saved warnings filters:", pf.prefcfg['warnings/filters'])
    if action:
        # Add filter for current session
        a, action = warningAction(action)
        if pf.debugon(pf.DEBUG.WARNING):
            print("Add current warnings filter: "
                  f"({action!r}, {message!r}, {category!r}, {module!r})")
        if message.isidentifier() or len(message) < 40:
            # Avoid short messages hiding longer ones starting with
            # the same string. Python filters test whether
            # the message starts with the given re.
            message += '$'
        warnings.filterwarnings(action, message, category, module)


def resetWarningFilters():
    """Reset the warning filters

    Reset the warning filters to the Python defaults plus the ones
    listed in the 'warnings/filters' configuration variable.
    """
    # Remove all pyFormex warnings
    warnings.filters = [f for f in warnings.filters
                        if not is_pyformex_warning(f[2])]
    warnings.simplefilter('default', PyformexWarning)
    warnings.simplefilter('always', PyformexRuntimeWarning)
    warnings.simplefilter('always', PyformexRuntimeError)
    if pf.debugon(pf.DEBUG.WARNING):
        print("Configured warning filters:", pf.cfg['warnings/filters'])
    # We always add the refcfg filters, to avoid having to store them
    # in the user's settings
    a = set(pf.refcfg['warnings/filters'])
    b = set(pf.prefcfg['warnings/filters'])
    for w in a | b:
        try:
            filter_warning(*w)
        except Exception:
            if pf.debugon(pf.DEBUG.WARNING):
                print("Error while processing warning filter:", w)
    if pf.debugon(pf.DEBUG.WARNING):
        print(f"Current {warnings.filters=}")


def printWarningFilters():
    """Print the current warning filters."""
    for f in warnings.filters:
        color = 'red' if f[2].__name__.startswith('Pyformex') else 'black'
        pf.printc(f, color=color)


def format_warning(message, category, filename, lineno, line=None):
    """Format a warning message.

    This acts as a replacement for :func:`warnings.formatwarning`.
    Its parameters have the same meaning, with the special cases
    mentioned below.

    Parameters
    ----------
    message: str | Warning
        The message to be shown. If *category* is PyformexWarning,
        the message is a simple mnemonic string and is looked up
        with :func:`messages.getMessage` to get the complete message.
        If the resulting message does not start with '..', it is
        decorated to create an ReST format message with a proper header.

    Examples
    --------
    >>> print(format_warning('test_message', PyformexWarning, 'unknown', 33))
    ..
    <BLANKLINE>
    pyFormex Warning
    ----------------
    **This is the warning shown to the user**
    <BLANKLINE>
    PyformexWarning called from: unknown, line: 33
    <BLANKLINE>
    """
    from pyformex import messages
    if isinstance(message, Warning):
        category = message.__class__
        message = str(message)
    if is_pyformex_warning(category) and not message.startswith('..'):
        message = '**' + messages.getMessage(message) + '**'
    name = getattr(category, 'name', category.__name__)
    message += f"\n\n{category.__name__} called from: {filename}, line: {lineno}"
    if pf.cfg['warnings/showline']:
        if line is None:
            try:
                import linecache
                line = linecache.getline(filename, lineno)
            except Exception:
                line = None
                linecache = None
        if line:
            message += f"::\n\n {line}"
    message += '\n'
    if not message.startswith('..'):
        message = forceRest(name) + '\n' + message
    return message


def show_warning(message, category, filename, lineno, file=None, line=None):
    """Replace the default warnings.showwarning

    We display the warnings using our interactive warning widget.
    This feature can be turned off by setting
    ``cfg['warnings/popup'] = False``
    """
    # expand the message with text from messages.py
    full_message = warnings.formatwarning(message, category, filename, lineno, line)
    if pf.GUI:
        if category == PyformexWarning:
            check = "Do not show this warning anymore in this and future sessions"
        elif category in [PyformexRuntimeWarning, RuntimeWarning]:
            check = "Do not show this warning anymore during this session"
        else:
            check = None
        level = 'error' if category == PyformexRuntimeError else 'warning'
        res = pf.GUI.popupMessage(full_message, level=level, check=check)
        if check:
            res, check = res
        save = check is not None and check[0]
        if category == PyformexWarning:
            action = 'ignore' if save else ''
            save = 'i' if save else ''
        elif category == PyformexRuntimeWarning:
            action = 'ignore' if save else ''
            save = ''
        else:
            action = ''
            save = ''
        if action or save:
            filter_warning(action, message, category, module='', save=save)
    else:
        print(full_message)


orig_formatwarning = warnings.formatwarning
orig_showwarning = warnings.showwarning

def set_warnings(nice=True, popup=True):
    """Sets how to show warnings"""
    warnings.formatwarning = format_warning if nice else orig_formatwarning
    warnings.showwarning = show_warning if popup else orig_showwarning


reset_warnings = functools.partial(set_warnings, nice=False, popup=False)

def warn(message, category=PyformexWarning, stacklevel=2, uplevel=0):
    """Create a warning through Python's warnings system.

    This is like :func:`warnings.warn`, but the default category is
    :class:`PyformexWarning` and it takes an extra uplevel argument
    whose value is added to the stacklevel (because it is easier to
    specify a relative level).

    Examples
    --------
    >>> warn('test_message')
    """
    if not pf._pytest:
        warnings.warn(message, category=category, stacklevel=stacklevel+uplevel)

# Specialized versions to set for user access
def warning(message, uplevel=0):
    """Create a pyFormex run time warning.

    This calls :func:`warn` with category :class:`PyformexRuntimeWarning`.
    """
    warn(message, category=PyformexRuntimeWarning, uplevel=uplevel+1)

def error(message, uplevel=0):
    """Create a pyFormex run time warning.

    This calls :func:`warn` with category :class:`PyformexRuntimeError`.
    """
    warn(message, category=PyformexRuntimeError, uplevel=uplevel+1)


def with_warning(message, category=PyformexWarning, uplevel=0):
    """Decorator to add a warning to a function.

    Adding this decorator to a function will warn the user with the
    supplied message when the decorated function gets executed for
    the first time in a session.
    An option is provided to switch off this warning in future sessions.

    This can be used as follows::

        @utils.with_warning('test_message')
        def func():
            ...
    """
    def decorator(func):
        def wrapper(*_args, **_kargs):
            warn(message, category=category, uplevel=uplevel+1)
            return func(*_args, **_kargs)
        functools.update_wrapper(wrapper, func)
        return wrapper
    return decorator


def deprecated_by(replace=None, *, uplevel=0):
    """Decorator to deprecate a function by another one.

    Adding this decorator to a function will warn the user with a
    message that the function is deprecated and should be replaced
    with  *replace*.
    """
    def decorator(func):
        def wrapper(*_args, **_kargs):
            message = f"{func.__qualname__} is deprecated."
            if replace:
                message += f" Use {replace} instead."
            warn(message, category=PyformexWarning, uplevel=1)
            return func(*_args, **_kargs)
        functools.update_wrapper(wrapper, func)
        return wrapper
    return decorator


def reset_funcs():
    pf.print = print
    pf.warning = warnings.warn
    pf.error = warnings.warn


##########################################################################
## Running external commands ##
###############################

# Store the outcome of the last command
last_command = None

def system(args, *, verbose=False, wait=True, **kargs):
    """Execute a command through the operating system.

    This is a wrapper around the :func:`process.run` function,
    aimed particularly at the pyFormex GUI user. It has one extra parameter:
    *verbose*. See :func:`process.run` for the other parameters.

    Parameters
    ----------
    verbose: bool
        If True, the command, and a report of its outcome in case of failure,
        timeout or error exit, are written to stdout.

    Returns
    -------
    :class:`DoneProcess` or subprocess.Popen
        If `wait` is True, returns a :class:`DoneProcess` with the outcome of
        the command.
        If `wait` is False, returns a subprocess.Popen which can be used to
        communicate with the started subprocess.

    See Also
    --------
    process.run: run a system command in a subprocess
    command: call :func:`system` with some other defaults

    Examples
    --------
    >>> P = system("pwd")
    >>> P.stdout.strip('\\n') == os.getcwd()
    True

    >>> P = system('true')
    >>> P
    DoneProcess(args=['true'], returncode=0, stdout='', stderr='')
    >>> P = system('false', capture_output=False)
    >>> P
    DoneProcess(args=['false'], returncode=1)
    >>> P = system('False', verbose=True)
    Running command: False
    DoneProcess report
    args: ['False']
    Command failed to run!
    returncode: 127

    >>> P = system("sleep 5", timeout=1, verbose=True)
    Running command: sleep 5
    DoneProcess report
    args: ['sleep', '5']
    returncode: -1
    stdout:
    stderr:
    timedout: True

    """
    global last_command

    if verbose:
        print(f"Running command: {args}")
        if pf.app:
            pf.app.processEvents()

    P = process.run(args, wait=wait, **kargs)

    if wait:
        last_command = P
        if verbose and (P.failed or P.timedout or P.returncode != 0):
            print(P)

    return P


def command(args, verbose=True, check=True, **kargs):
    """Run an external command in a user friendly way.

    This is equivalent with :func:`system` with verbose=True by default.
    """
    return system(args, verbose=verbose, **kargs)


def killProcesses(pids, signal=15):
    """Send the specified signal to the processes in list

    Parameters
    ----------
    pids: list of int
        List of process ids to be killed.
    signal: int
        Signal to be send to the processes. The default (15) will
        try to terminate the process in a friendly way.
        See ``man kill`` for more values.
    """
    for pid in pids:
        try:
            os.kill(pid, signal)
        except Exception:
            if pf.debugon(pf.DEBUG.INFO):
                print("Error in killing of process", pid)


# NOT USED.
# def execSource(script, glob={}):
#     """Execute Python code in another thread.

#     Parameters
#     ----------
#     script: str
#         A string containing some executable Python/pyFormex code.
#     glob: dict, optional
#         A dict with globals specifying the environment in which the
#         source code is executed.
#     """
#     pf.interpreter.locals = glob
#     pf.interpreter.runsource(script, '<input>', 'exec')


##########################################################################
## match multiple patterns
##########################


def matchMany(regexps, target):
    """Return multiple regular expression matches of the same target string."""
    return [re.match(r, target) for r in regexps]


def matchCount(regexps, target):
    """Return the number of matches of target to  regexps."""
    return len([_f for _f in matchMany(regexps, target) if _f])


def matchAny(regexps, target):
    """Check whether target matches any of the regular expressions."""
    return matchCount(regexps, target) > 0


def matchNone(regexps, target):
    """Check whether target matches none of the regular expressions."""
    return matchCount(regexps, target) == 0


def matchAll(regexps, target):
    """Check whether targets matches all of the regular expressions."""
    return matchCount(regexps, target) == len(regexps)


def okURL(url):
    """Check that an URL is displayable in the browser.

    Parameters
    ----------
    url: URL
        The URL to be checked.

    Returns
    -------
    bool
        True if ``url`` starts with a protocol that is either
        'http:', 'https:' or 'file:'; else False
    """
    s = url.split(':')
    return len(s) > 1 and s[0] in ['http', 'https', 'file']


##########################################################################
## Filenames ##
###############

# TODO: these should go to path module


def projectName(fn):
    """Derive a project name from a file name.

    The project name is the basename of the file without the extension.
    It is equivalent with Path(fn).stem

    Examples
    --------
    >>> projectName('aa/bb/cc.dd')
    'cc'
    >>> projectName('cc.dd')
    'cc'
    >>> projectName('cc')
    'cc'
    """
    return Path(fn).stem


def findIcon(name):
    """Return the file name for an icon with given name.

    Parameters
    ----------
    name: str
        Name of the icon: this is the stem fof the filename.

    Returns
    -------
    str
        The full path name of an icon file with the specified name, found
        in the pyFormex icon folder, or the question mark icon file, if
        no match was found.

    Examples
    --------
    >>> print(findIcon('view-xr-yu').relative_to(pf.cfg['pyformexdir']))
    icons/view-xr-yu.xpm
    >>> print(findIcon('right').relative_to(pf.cfg['pyformexdir']))
    icons/64x64/right.png
    >>> print(findIcon('xyz').relative_to(pf.cfg['pyformexdir']))
    icons/question.xpm
    >>> print(findIcon('recording').relative_to(pf.cfg['pyformexdir']))
    icons/recording.gif
    """
    for icondir in pf.cfg['gui/icondirs']:
        for icontype in pf.cfg['gui/icontypes']:
            fname = icondir / (name+icontype)
            if fname.exists():
                return fname

    return pf.cfg['icondir'] / 'question.xpm'


def listIconNames(dirs=None, types=None):
    """Return the list of available icons by their name.

    Parameters
    ----------
    dirs: list of paths, optional
        If specified, only return icons names from these directories.
    types: list of strings, optional
        List of file suffixes, each starting with a dot. If specified,
        Only names of icons having one of these suffixes are returned.

    Returns
    -------
    list of str
        A sorted list of the icon names available in the pyFormex icons folder.

    Examples
    --------
    >>> listIconNames()[:4]
    ['clock', 'dist-angle', 'down', 'down']
    >>> listIconNames([pf.cfg['icondir'] / '64x64'])[:4]
    ['down', 'ff', 'info', 'lamp']
    >>> listIconNames(types=['.xpm'])[:4]
    ['clock', 'dist-angle', 'down', 'empty']
    """
    if dirs is None:
        dirs = pf.cfg['gui/icondirs']
    if types is None:
        types = pf.cfg['gui/icontypes']
    types = ['.+\\'+t for t in types]
    icons = []
    for icondir in dirs:
        icons += [Path(f).stem for f in icondir.filenames()
                  if matchAny(types, f)]
    return sorted(icons)


##########################################################################
## File lists ##
################


def sourceFiles(relative=False, symlinks=True, extended=False,
                includedir=None):
    """Return the list of pyFormex .py source files.

    Parameters
    ----------
    relative: bool
        If True, returned filenames are relative to the current directory.
    symlinks: bool
        If False, files that are symbolic links are retained in the
        list. The default is to remove them.
    extended: bool
        If True, also return the .py files in all the paths in the configured
        appdirs and scriptdirs.

    Returns
    -------
    list of str
        A list of filenames of .py files in the pyFormex source tree, and,
        if ``extended`` is True, .py files in the configured app and script
        dirs as well.
    """
    path = pf.cfg['pyformexdir']
    ftypes = 'Hf' if symlinks else 'HSf'
    if includedir is None:
        includedir = pf.cfg['sourcedirs']
    if relative:
        path = path.relative_to(Path.cwd())
    files = path.listTree(
        includedir=includedir,
        includefile=[r'.*\.py', 'pyformexrc'],
        ftypes=ftypes)
    if extended:
        searchdirs = [Path(i[1]) for i in pf.cfg['appdirs'] + pf.cfg['scriptdirs']]
        for path in set(searchdirs):
            if path.exists():
                files += path.listTree(
                    includefile=[r'.*\.py'], ftypes=ftypes)
    return files


def grepSource(pattern, options='', relative=True, verbose=False):
    """Finds pattern in the pyFormex source files.

    Uses the `grep` program to find all occurrences of some specified
    pattern text in the pyFormex source .py files (including the examples).
    Extra options can be passed to the grep command. See `man grep` for
    more info.

    Returns the output of the grep command.
    """
    opts = options.split(' ')
    if '-a' in opts:
        opts.remove('-a')
        options = ' '.join(opts)
        extended = True
    else:
        extended = False
    files = sourceFiles(relative=relative, extended=extended, symlinks=False)
    cmd = f"grep {options} '{pattern}' {' '.join(files)}"
    P = system(cmd, verbose=verbose)
    if not P.returncode:
        return P.stdout


def findModuleSource(module):
    """Find the path of the source file of a module

    module is either an imported module (pkg.mod) or a string with the module
    name ('pkg.mod'), imported or not.
    Returns the source file from which the module was/would be loaded when
    imported.
    Raises an error if the module can not be imported or
    does not have a source file.
    """
    import importlib.util
    if isinstance(module, str):
        spec = importlib.util.find_spec(module)
        if spec is None:
            raise ImportError(f"Can't find module {module}")
    else:
        spec = module.__spec__
    return spec.origin


def humanSize(size, units, ndigits=-1):
    """Convert a number to a human size.

    Large numbers are often represented in a more human readable
    form using k, M, G prefixes. This function returns the input
    size as a number with the specified prefix.

    Parameters
    ----------
    size: int or float
        A number to be converted to human readable form.
    units: str
        A string specifying the target units. The first character should
        be one of k,K,M,G,T,P,E,Z,Y. 'k' and 'K' are equivalent. A second
        character 'i' can be added to use binary (K=1024) prefixes instead of
        decimal (k=1000).
    ndigits: int, optional
        If provided and >=0, the result will be rounded to this number of
        decimal digits.

    Returns
    -------
    float
        The input value in the specified units and possibly rounded
        to ``ndigits``.

    Examples
    --------
    >>> humanSize(1234567890,'k')
    1234567.89
    >>> humanSize(1234567890,'M',0)
    1235.0
    >>> humanSize(1234567890,'G',3)
    1.235
    >>> humanSize(1234567890,'Gi',3)
    1.15
    """
    size = float(size)
    order = '.KMGTPEZY'.find(units[0].upper())
    if units[1:2] == 'i':
        scale = 1024.
    else:
        scale = 1000.
    size = size / scale**order
    if ndigits >= 0:
        size = round(size, ndigits)
    return size


###################### locale ###################

def setSaneLocale(localestring=''):
    """Set a sane local configuration for LC_NUMERIC.

    `localestring` is the locale string to be set, e.g. 'en_US.UTF-8' or
    'C.UTF-8' for no locale.

    Sets the ``LC_ALL`` locale to the specified string if that is not empty,
    and (always) sets ``LC_NUMERIC`` and ``LC_COLLATE`` to 'C.UTF-8'.

    Changing the LC_NUMERIC setting is a very bad idea! It makes floating
    point values to be read or written with a comma instead of a the decimal
    point. Of course this makes input and output files completely incompatible.
    You will often not be able to process these files any further and
    create a lot of troubles for yourself and other people if you use an
    LC_NUMERIC setting different from the standard.

    Because we do not want to help you shoot yourself in the foot, this
    function always sets ``LC_NUMERIC`` back to a sane 'C' value and we
    call this function when pyFormex is starting up.
    """
    import locale
    if localestring:
        locale.setlocale(locale.LC_ALL, localestring)
    locale.setlocale(locale.LC_ALL, 'C.UTF-8')


##########################################################################
## Text conversion  tools ##
############################


def rreplace(source, old, new, count=1):
    """Replace substrings starting from the right.

    Replaces count occurrences of old substring with a new substring.
    This is like str.replace, but counting starts from the right.
    The default count=1 replaces only the last occurrence,
    and is identical to str.replace (which is then preferred).

    Parameters
    ----------
    source: str
        The input string. It can be subclass of str, e.g. :class:`~path.Path`.
    old: str
        The substring to be replaced.
    new: str
        The string to replace old.
    count: int
        The maximum number of replacements.

    Returns
    -------
    str
        The string with the replacements made. If source was a subclass
        of str, the returned string will be of the same subclass.

    Examples
    --------
    >>> print(rreplace('abababa', 'ab', '+de'))
    abab+dea
    >>> print(rreplace('abababa', 'ab', '+de', 2))
    ab+de+dea
    >>> for i in (0, 1, 2, 3, 4, -1):
    ...     print(f"{i}: {rreplace('abcabcabc', 'ab', '-ef', i)}")
    0: abcabcabc
    1: abcabc-efc
    2: abc-efc-efc
    3: -efc-efc-efc
    4: -efc-efc-efc
    -1: -efc-efc-efc
    >>> rreplace(Path('dirname/filename.ext'), 'name.e', 'name00.n')
    Path('dirname/filename00.nxt')
    """
    return source.__class__(new.join(source.rsplit(old, count)))


def strNorm(s):
    """Normalize a string.

    Text normalization removes all '&' characters and converts it to lower case.

    >>> strNorm("&MenuItem")
    'menuitem'

    """
    return str(s).replace('&', '').lower()


_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.:]+')


def slugify(text, delim='-'):
    """Convert a string into a URL-ready readable ascii text.

    Examples
    --------
    >>> slugify("http://example.com/blog/[Some] _ Article's Title--")
    'http-example-com-blog-some-article-s-title'
    >>> slugify("&MenuItem")
    'menuitem'
    """
    import unicodedata
    text = str(text)
    result = []
    for word in _punct_re.split(text.lower()):
        word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore')
        if word:
            result.append(bytes.decode(word))
    return delim.join(result)

###################### text formatting ###################



def sprint(*args, **kargs):
    """Print to a string

    This takes all the parameters as :func:`print` but returs the result in
    a string.
    """
    with io.StringIO() as output:
        print(*args, file=output, **kargs)
        return output.getvalue()


def isTextFile(fn):
    """Check that a file is a text file"""
    cmd = ['file', fn]
    P = system(cmd)
    return P.returncode == 0 and 'text' in P.stdout


if not pf._sphinx and Module.has('docutils', quiet=True):
    def rst2html(text, writer='html'):
        from docutils.core import publish_string
        # warnings.filterwarnings("ignore", category=DeprecationWarning)
        try:
            s = publish_string(text, writer_name=writer)
        except Exception as e:
            print(e)
            return text
        return s
        # warnings.filterwarnings("default", category=DeprecationWarning)
else:
    def rst2html(text, writer='html'):
        return """.. note::
   This is a reStructuredText message, but it is currently displayed
   as plain text, because it could not be converted to html.
   If you install python-docutils, you will see this text (and other
   pyFormex messages) in a much nicer layout!

""" + text


def textFormat(text):
    """Detect text format

    Parameters
    ----------
    text: str
        A multiline string in one of the supported formats:
        plain text, html, rest, markdown

    Returns
    -------
    format: str
        The detected format: one of 'plain', 'html', 'rest' or 'markdown'

    Examples
    --------
    >>> textFormat('''..
    ...     Header
    ...     ------
    ... ''')
    'rest'
    """
    if text.startswith('<'):
        format = 'html'
    elif text.startswith('..'):
        format = 'rest'
    elif text.startswith('\n[comment]: #'):
        format = 'markdown'
    else:
        format = 'plain'
    return format


def convertText(text, format=''):
    """Convert a text to a format recognized by Qt.

    Input text format is plain, rest, markdown or html.
    Output text format is plain, markdown or html, with rest being
    converted to html.

    Parameters
    ----------
    text: str
        A multiline string in one of the supported formats:
        plain text, html or reStructuredText.
    format: str, optional
        The format of the text: one of 'plain', 'html' ot 'rest'. The
        default '' will autorecognize the supported formats.

    Returns
    -------
    text: str
        The converted text, being either plain markdown or html.
    format: str
        The output format: 'plain', 'markdown' or 'html'

    Notes
    -----
    For the conversion of reStructuredText to work,
    the Python docutils have to be installed on the system.

    Examples
    --------
    >>> convertText('''..
    ...     Header
    ...     ------
    ... ''')[0].startswith('<?xml')
    True
    """
    if format not in ['plain', 'html', 'rest', 'markdown']:
        format = textFormat(text)
    # conversion
    if format == 'rest' and pf.cfg['gui/rst2html']:
        # Try conversion to html
        text = rst2html(text)
        format = 'html'

    if isinstance(text, bytes):
        text = text.decode()

    return text, format


def forceRest(text, underline=True):
    """Convert a text string to have it recognized as reStructuredText.

    Parameters
    ----------
    text: str
        A multiline string with some text that is formated as
        reStructuredText.
    underline: bool
        If True (default), the first line of the text will be underlined to make
        it a header in the reStructuredText.

    Returns
    -------
    str:
        The input text with two lines prepended: a line with '..'
        and a blank line. The pyFormex text display functions will then
        recognize the text as being reStructuredText.
        Since the '..' starts a comment in reStructuredText, the extra
        lines are not displayed. If underline=True, an extra line is
        added below the (original) first line, to make that line appear
        as a header.

    Examples
    --------
    >>> print(forceRest('Header\\nBody', underline=True))
    ..
    <BLANKLINE>
    Header
    ------
    Body

    """
    if underline:
        text = underlineHeader(text)
    return "..\n\n" + text


def underlineHeader(s, c='-'):
    """Underline the first line of a text.

    Parameters
    ----------
    s: str
        A multiline string.
    c: char, optional
        The character to use for underlining. Default is '-'.

    Returns
    -------
    str:
        A multiline string with the original text plus an extra line
        inserted below the first line. The new line has the same length
        as the first, but all characters are equal to the specified char.

    Examples
    --------
    >>> print(underlineHeader("Hello World"))
    Hello World
    -----------
    """
    i = s.find('\n')
    if i < 0:
        i = len(s)
    return s[:i] + '\n' + c*i + s[i:]


def indent(text, n=2):
    """Indent the lines of text"""
    import textwrap
    return textwrap.indent(text, ' ' * n)


def indentTail(text, n=2):
    """Indent the lines of text except the first one"""
    first, *tail = text.split('\n', 1)
    if tail:
        return first + '\n' + indent(tail[0], n)
    else:
        # No '\n' in text
        return first


def sameLength(lines, length=-1, adjust='l'):
    """Make a sequence of strings the same length.

    Parameters
    ----------
    lines: list of str
        A sequence of single line strings.
    length: int
        The required length of the lines. If negative, the length is
        set to the maximum input line length.
    adjust: 'l' | 'c' | 'r'
        How the input lines are adjusted to respectively the left,
        the center or the right of the total length of the line.

    Examples
    --------
    >>> sameLength(['a', 'bb', 'ccc'])
    ['a  ', 'bb ', 'ccc']
    >>> sameLength(['a', 'bb', 'ccc'], adjust='c')
    [' a ', ' bb', 'ccc']
    >>> sameLength(['a', 'bb', 'ccc'], adjust='r')
    ['  a', ' bb', 'ccc']
    >>> sameLength(['a', 'bb', 'ccc'], length=2)
    ['a ', 'bb', 'cc']
    """
    if length < 0:
        length = max([len(l) for l in lines])
    _adjust = {'c': str.center, 'l': str.ljust, 'r': str.rjust}[adjust]
    return [_adjust(line, length) if len(line) <= length else line[:length]
            for line in lines]


def framedText(text,
               padding=[0, 2, 0, 2], border=[1, 2, 1, 2], margin=[0, 0, 0, 0],
               borderchar='####', adjust='l'):
    """Create a text with a frame around it.

    Parameters
    ----------
    padding: list of int
        Number of blank spaces around text, at the top, right, bottom, left.
    border: list of int
        Border width, at the top, right, bottom, left.
    margin: list of int
        Number of blank spaces around border, at the top, right, bottom, left.
    borderchar: str
        Border charater, at the top, right, bottom, left.
    width: int
        Intended width of the
    adjust: 'l' | 'c' | 'r'
        Adjust the text to the left, center or right.

    Returns
    -------
    str:
        A multiline string with the formatted framed text.

    Examples
    --------
    >>> print(framedText("Hello World,\\nThis is me calling",adjust='c'))
    ##########################
    ##     Hello World,     ##
    ##  This is me calling  ##
    ##########################
    >>> print(framedText("Hello World,\\nThis is me calling",margin=[1,0,0,3]))
    <BLANKLINE>
       ##########################
       ##  Hello World,        ##
       ##  This is me calling  ##
       ##########################
    """
    lines = text.splitlines()
    maxlen = max([len(l) for l in lines])
    lines = sameLength(lines, maxlen)
    prefix = borderchar[3]*border[3] + ' '*padding[3]
    suffix = ' '*padding[1] + borderchar[1]*border[1]
    width = len(prefix) + maxlen + len(suffix)
    lmargin = ' '*margin[3]
    prefix = lmargin + prefix
    paddingline = prefix + ' '*maxlen + suffix
    borderline = lmargin + borderchar[0]*width
    s = []
    for i in range(margin[0]):
        s.append('')
    for i in range(border[0]):
        s.append(borderline)
    for i in range(padding[0]):
        s.append(paddingline)
    for l in lines:
        s.append(prefix + l + suffix)
    for i in range(padding[2]):
        s.append(paddingline)
    for i in range(border[2]):
        s.append(borderline)
    for i in range(margin[2]):
        s.append('')
    return '\n'.join(s)


def prefixText(text, prefix):
    """Add a prefix to all lines of a text.

    Parameters
    ----------
    text: str
        A multiline string with the input text.
    prefix: str
        A string to be inserted at the start of all lines of text.

    Returns
    -------
    str
        A multiline string with the input lines prefixed with prefix.

    Examples
    --------
    >>> print(prefixText("line1\\nline2","** "))
    ** line1
    ** line2

    """
    return '\n'.join([prefix+line for line in text.split('\n')])



##########################################################################
##  Miscellaneous            ##
###############################

def userName():
    """Find the name of the user."""
    import getpass
    return getpass.getuser()


def isString(o):
    """Test if an object is a string (ascii or unicode)"""
    return isinstance(o, (str, bytes))


def isFile(o):
    """Test if an object is a file"""
    return isinstance(o, io.IOBase)


def is_script(appname):
    """Checks whether an application name is rather a script file name

    Parameters
    ----------
    appname: str
        The name of a script file or an app.

    Returns
    -------
    bool:
        True if appname ends with '.py', or contains a '/'.
    """
    appname = str(appname)
    return appname.endswith('.py') or '/' in appname


######################## Useful classes ##################

class Counter:
    def __init__(self, start=0, step=1):
        self.nr = start
        self.step = step

    def peek(self):
        return self.nr

    def __next__(self):
        nr = self.nr
        self.nr += self.step
        return nr

    __call__ = __next__


def prefixDict(d, prefix=''):
    """Prefix all the keys of a dict with the given prefix.

    Parameters
    ----------
    d: dict
        A dict where all keys are strings.
    prefix: str
        A string to prepend to all keys in the dict.

    Returns
    -------
    dict
        A dict with the same contents as the input, but where all keys
        have been prefixed with the given prefix string.

    Examples
    --------
    >>> prefixDict({'a':0,'b':1},'p_')
    {'p_a': 0, 'p_b': 1}
    """
    return dict([(prefix+k, d[k]) for k in d])


def subDict(d, prefix='', strip=True, remove=False):
    """Return a dict with the items whose key starts with prefix.

    Parameters
    ----------
    d: dict
        A dict where all the keys are strings.
    prefix: str
        The string that is to be found at the start of the keys.
    strip: bool
        If True (default), the prefix is stripped from the keys.

    Returns
    -------
    dict
        A dict with all the items from ``d`` whose key starts
        with ``prefix``. The keys in the returned dict will have the prefix
        stripped off, unless strip=False is specified.

    Examples
    --------
    >>> subDict({'p_a':0,'q_a':1,'p_b':2}, 'p_')
    {'a': 0, 'b': 2}
    >>> subDict({'p_a':0,'q_a':1,'p_b':2}, 'p_', strip=False)
    {'p_a': 0, 'p_b': 2}
    >>> a = {'p_a':0,'q_a':1,'p_b':2}
    >>> b = subDict(a, 'p_', remove=True, strip=False)
    >>> a, b
    ({'q_a': 1}, {'p_a': 0, 'p_b': 2})
    """
    keys = [k for k in d if k.startswith(prefix)]
    if strip:
        subd = dict([(k.replace(prefix, '', 1), d[k]) for k in keys])
    else:
        subd = dict([(k, d[k]) for k in keys])
    if remove:
        for k in keys:
            del d[k]
    return subd


def selectDict(d, keys, remove=False):
    """Return a dict with the items whose key is in keys.

    Parameters
    ----------
    d: dict
        The dict to select items from.
    keys: set of str
        The keys to select from ``d``. This can be a set or list of key
        values, or another dict, or any object having the ``key in object``
        interface.
    remove: bool
        If True, the selected keys are removed from the input dict.

    Returns
    -------
    dict
        A dict with all the items from ``d`` whose key is in ``keys``.

    See Also
    --------
    removeDict: the complementary operation, returns items not in ``keys``.

    Examples
    --------
    >>> d = dict([(c,c*c) for c in range(4)])
    >>> print(d)
    {0: 0, 1: 1, 2: 4, 3: 9}
    >>> selectDict(d,[2,0])
    {2: 4, 0: 0}
    >>> print(d)
    {0: 0, 1: 1, 2: 4, 3: 9}
    >>> selectDict(d,[2,0,6],remove=True)
    {2: 4, 0: 0}
    >>> print(d)
    {1: 1, 3: 9}
    """
    keys = [k for k in keys if k in d]
    sel = dict([(k, d[k]) for k in keys])
    if remove:
        for k in keys:
            del d[k]
    return sel


def removeDict(d, keys):
    """Return a dict with the specified keys removed.

    Parameters
    ----------
    d: dict
        The dict to select items from.
    keys: set of str
        The keys to select from ``d``. This can be a set or list of key
        values, or another dict, or any object having the ``key in object``
        interface.

    Returns
    -------
    dict
        A dict with all the items from ``d`` whose key is not in ``keys``.

    See Also
    --------
    selectDict: the complementary operation returning the items in ``keys``

    Examples
    --------
    >>> d = dict([(c,c*c) for c in range(6)])
    >>> removeDict(d,[4,0])
    {1: 1, 2: 4, 3: 9, 5: 25}
    """
    return dict([(k, d[k]) for k in set(d)-set(keys)])


def mutexkeys(d, keys):
    """Enforce a set of mutually exclusive keys in a dictionary.

    This makes sure that d has only one of the specified keys.
    It modifies the dictionary inplace.

    Parameters
    ----------
    d: dict
        The input dictionary.
    keys:
        A list of dictionary keys that are mutually exclusive.

    Examples
    --------
    >>> d = {'a':0, 'b':1, 'c':2}
    >>> mutexkeys(d, ['b', 'c', 'a'])
    >>> print(d)
    {'b': 1}
    """
    keep = None
    for k in keys:
        if k in d:
            keep = k
            break
    if keep:
        for k in keys:
            if k != keep:
                d.pop(k, None)


def refreshDict(d, src):
    """Refresh a dict with values from another dict.

    The values in the dict d are update with those in src.
    Unlike the dict.update method, this will only update existing keys
    but not add new keys.
    """
    d.update(selectDict(src, d))


def inverseDict(d):
    """Return the inverse of a dictionary.

    Returns a dict with keys and values interchanged.

    Example:

    >>> inverseDict({'a':0,'b':1})
    {0: 'a', 1: 'b'}
    """
    return dict([(d[k], k) for k in d])


def selectDictValues(d, values):
    """Return the keys in a dict which have a specified value

    - `d`: a dict where all the keys are strings.
    - `values`: a list/set of values.

    The return value is a list with all the keys from d whose value
    is in keys.

    Example:

    >>> d = dict([(c,c*c) for c in range(6)])
    >>> selectDictValues(d,range(10))
    [0, 1, 2, 3]
    """
    return [k for k in d if d[k] in values]


class DictDiff():
    """A class to compute the difference between two dictionaries

    Parameters
    ----------
    current_dict: dict
    past_dict: dict

    The differences are reported as sets of keys:
    - items added
    - items removed
    - keys same in both but changed values
    - keys same in both and unchanged values
    """
    def __init__(self, current_dict, past_dict):
        self.current_dict, self.past_dict = current_dict, past_dict
        self.current_keys, self.past_keys = [
            set(d.keys()) for d in (current_dict, past_dict)]
        self.intersect = self.current_keys.intersection(self.past_keys)

    def added(self):
        """Return the keys in current_dict but not in past_dict"""
        return self.current_keys - self.intersect

    def removed(self):
        """Return the keys in past_dict but not in current_dict"""
        return self.past_keys - self.intersect

    def changed(self):
        """Return the keys for which the value has changed"""
        return set(o for o in self.intersect
                   if self.past_dict[o] != self.current_dict[o])

    def unchanged(self):
        """Return the keys with same value in both dicts"""
        return set(o for o in self.intersect
                   if self.past_dict[o] == self.current_dict[o])


    def equal(self):
        """Return True if both dicts are equivalent"""
        return len(self.added() | self.removed() | self.changed()) == 0


    def report(self):
        """Create a reports of the differences"""
        return (f"""Dict difference report:
    (1) items added : {', '.join(self.added())}
    (2) items removed : {', '.join(self.removed())}
    (3) keys same in both but changed values : {', '.join(self.changed())}
    (4) keys same in both and unchanged values : {', '.join(self.unchanged())}
""")


###################### file conversion ###################


def dos2unix(infile):
    """Convert a text file to unix line endings."""
    return system(f"sed -i 's|$|\\r|' {infile}")


def unix2dos(infile, outfile=None):
    """Convert a text file to dos line endings."""
    return system(f"sed -i 's|\\r||' {infile}")


def countLines(fn):
    """Return the number of lines in a text file."""
    P = system(["wc", fn])
    if P.returncode == 0:
        return int(P.stdout.split()[0])
    else:
        return 0

###########################################################
## fonts ##

def listFonts(pattern='', include=None, exclude=None):
    """List the fonts known to the system.

    This uses the 'fc-list' command from the fontconfig package to find a list
    of font files installed on the user's system. The list of files can be
    restricted by three parameters: a pattern to be passed to the fc-list
    command, an include regexp specifying which of the matching font files
    should be retained, and an exclude regexp specifying which files
    should be removed from the remaining list.

    Parameters
    ----------
    pattern: str
        A pattern string to pass to the fc-list command. For example,
        a pattern 'mono' will only list monospaced fonts. Multiple
        elements can be combined with a colon as separator. Example:
        pattern='family=DejaVuSans:style=Bold'. An empty string selects
        all font files.
    include: str
        Regex for grep to select the font files to include in the result.
        If not specified, the pattern from the configuration variable
        'fonts/include' is used. Example: the default configured
        include='\\.ttf$' will only return font files with a .ttf suffix.
        An empty string will include all files selected by the ``pattern``.
    exclude: str
        Regex for grep to select the font files to include in the result.
        If not specified, the pattern from the configuration variable
        'fonts/include' is used. Example: the default configured
        exclude='Emoji' will exclude font files that have 'Emoji' in their name.
        An empty string will exclude no files.

    Returns
    -------
    list of :class:`Path`
        A list of the font files found on the system. If fontconfig is not
        installed, produces a warning and returns an empty list.

    Examples
    --------
    >>> fonts = listFonts('mono')
    >>> print(len(fonts) > 0 and fonts[0].is_file())
    True
    """
    if include is None:
        include = pf.cfg['fonts/include']
    if exclude is None:
        exclude = pf.cfg['fonts/exclude']
    cmd = f"fc-list :{pattern} file | sed 's|.*file=||;s|:.*||;s| ||g'"
    if include:
        cmd += f" | grep '{include}'"
    if exclude:
        cmd += f" |grep -v '{exclude}'"
    cmd += " | sort"
    P = system(cmd, shell=True)
    if P.returncode:
        pf.warning("fc-list could not find your font files.\n"
                   "Maybe you do not have fontconfig installed?")
        fonts = []
    else:
        fonts = [Path(f) for f in P.stdout.split('\n') if f]
    return fonts


def listMonoFonts():
    """List the monospace fonts found on the system

    This is equivalent to ``listFonts('mono')``

    See Also
    --------
    listFonts
    """
    return listFonts('mono')


def defaultMonoFont():
    """Return a default monospace font for the system.

    Returns
    -------
    :class:`Path`
        If the configured 'fonts/default' has a matching font file on the
        system, that  Path is returned. Else, the first file from
        ``fontList('mono')`` is returned.

    Raises
    ------
    ValuerError
        If no monospace font was found on the system

    Examples
    --------
    >>> print(defaultMonoFont())
    /...DejaVuSansMono.ttf
    """
    for pattern in (f"fullname={pf.cfg['fonts/default']}", 'mono'):
        fonts = listFonts(pattern)
        if len(fonts) > 0:
            font = fonts[0]
            if font.is_file():
                return font
    raise ValueError(
        "I could not find any monospace font file on your system")


##################################################################
## misc ##

def execFile(f, *args, **kargs):
    with open(f, 'r') as fil:
        return exec(compile(fil.read(), f, 'exec'), *args, **kargs)


def interrogate(item):
    """Print useful information about item."""
    info = {}
    if hasattr(item, '__name__'):
        info["NAME:    "] = item.__name__
    if hasattr(item, '__class__'):
        info["CLASS:   "] = item.__class__.__name__
    info["ID:      "] = id(item)
    info["TYPE:    "] = type(item)
    info["VALUE:   "] = repr(item)
    info["CALLABLE:"] = callable(item)
    if hasattr(item, '__doc__'):
        doc = getattr(item, '__doc__')
        doc = doc.strip()   # Remove leading/trailing whitespace.
        firstline = doc.split('\n')[0]
        info["DOC:     "] = firstline
    for k in info:
        print(f"{k} {info[k]}")


def memory_report(keys=None):
    """Return info about memory usage"""
    import gc
    gc.collect()
    P = system('cat /proc/meminfo')
    res = {}
    for line in str(P.stdout).split('\n'):
        try:
            k, v = line.split(':')
            k = k.strip()
            v = v.replace('kB', '').strip()
            res[k] = int(v)
        except Exception:
            break
    res['MemUsed'] = res['MemTotal'] - res['MemFree'] - res['Buffers'] - res['Cached']
    if keys:
        res = selectDict(res, keys)
    return res


def memory_diff(mem0, mem1, tag=None):
    m0 = mem0['MemUsed']/1024.
    m1 = mem1['MemUsed']/1024.
    m2 = m1 - m0
    m3 = mem1['MemFree']/1024.
    print(f"{m0:10.1f} MB before; {m1:10.1f} MB after; "
          f"{m2:10.1f} MB used; {m3:10.1f} MB free; {tag}")


_mem_state = None


def memory_track(tag=None, since=None):
    global _mem_state
    if since is None:
        since = _mem_state
    new_mem_state = memory_report()
    if tag and _mem_state is not None:
        memory_diff(since, new_mem_state, tag)
    _mem_state = new_mem_state
    return _mem_state


def totalMemSize(o, handlers={}, verbose=False):
    """Return the approximate total memory footprint of an object.

    This function returns the approximate total memory footprint of an
    object and all of its contents.

    Automatically finds the contents of the following builtin containers and
    their subclasses:  tuple, list, deque, dict, set and frozenset.
    To search other containers, add handlers to iterate over their contents::

        handlers = {SomeContainerClass: iter,
                    OtherContainerClass: OtherContainerClass.get_elements}

    Adapted from http://code.activestate.com/recipes/577504/
    """
    import sys
    from itertools import chain
    from collections import deque
    try:
        from reprlib import repr
    except ImportError:
        pass

    def dict_handler(d):
        return chain.from_iterable(d.items())

    all_handlers = {
        tuple: iter,
        list: iter,
        deque: iter,
        dict: dict_handler,
        set: iter,
        frozenset: iter,
    }
    all_handlers.update(handlers)  # user handlers take precedence
    seen = set()                # track which object id's have already been seen
    default_size = sys.getsizeof(0)  # estimate sizeof object without __sizeof__

    def sizeof(o):
        if id(o) in seen:       # do not double count the same object
            return 0
        seen.add(id(o))
        s = sys.getsizeof(o, default_size)

        if verbose:
            print(s, type(o), repr(o), file=sys.stderr)

        for typ in all_handlers:
            if isinstance(o, typ):
                handler = all_handlers[typ]
                s += sum([sizeof(i) for i in handler(o)])
                break
        return s

    return sizeof(o)


def memUsed():
    return memory_report()['MemUsed']


def disk_usage(paths):
    """Estimate the amount of disk space used by files

    Parameters
    ----------
    paths: list of :term:`path_like`
        A list of file or directory paths. Tilde expand is applied on the
        path names.

    Returns
    -------
    dict
        A dict where each key is a path name and the value is the estimate
        disk space used by it. For a directory path, the result is the total
        recursive size for the directory.
        Inaccessible paths are silently ignored, so the number of results can
        be smaller than the **paths** list.
        Symbolic links are not followed, except those specified in **paths**.

    Examples
    --------
    >>> print(disk_usage([pf.cfg['pyformexdir'], '/etc/hosts']))
    {'.../pyformex': ..., '/etc/hosts': ...}
    """
    if not isinstance(paths, list):
        paths = [paths]
    cmd = ['du', '-B1', '-s', '-D'] + [os.path.expanduser(p) for p in paths]
    P = system(cmd)
    out = {}
    for line in P.stdout.split('\n'):
        s = line.split('\t')
        if len(s) == 2:
            out[s[1]] = int(s[0])
    return out

################ Special import mechanisms ##############

def import_lazy(name):
    import sys
    import importlib.util
    spec = importlib.util.find_spec(name)
    loader = importlib.util.LazyLoader(spec.loader)
    spec.loader = loader
    module = importlib.util.module_from_spec(spec)
    sys.modules[name] = module
    loader.exec_module(module)
    return module

def import_direct(file):
    import sys
    import importlib.util
    import tokenize
    file_path = tokenize.__file__
    module_name = tokenize.__name__
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    return module

def compile_and_dis(src):
    from dis import dis
    dis(compile(src, '<string>', 'exec'))


### End
