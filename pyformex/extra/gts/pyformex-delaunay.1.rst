..

..
  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
  SPDX-License-Identifier: GPL-3.0-or-later

  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
  pyFormex is a tool for generating, manipulating and transforming 3D
  geometrical models by sequences of mathematical operations.
  Home page: https://pyformex.org
  Project page: https://savannah.nongnu.org/projects/pyformex/
  Development: https://gitlab.com/bverheg/pyformex
  Distributed under the GNU General Public License version 3 or later.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/.


=================
pyformex-delaunay
=================

----------------------------------------------
Construct a constrained Delaunay triangulation
----------------------------------------------

:Author: Benedict Verhegghe <bverheg@gamil.com>. This manual page was written for the pyFormex project (and may be used by others).
:Date:   2024-04-04
:Copyright: GPL v3 or higher
:Version: 0.2
:Manual section: 1
:Manual group: text and X11 processing

SYNOPSIS
========

pyformex-delaunay [OPTION] < file.gts

DESCRIPTION
===========

Construct the constrained Delaunay triangulation of the input.

OPTIONS
=======

-b, --hull               Do not keep convex hull
-e, --noconst            Do not add constrained edges
-S, --split              Split constraints (experimental)
-H, --holes              Remove holes from the triangulation
-d, --duplicates         Remove duplicate vertices
-r, --randomize          Shuffle input vertex list
-c, --check              Check Delaunay property
-f FNAME, --files=FNAME  Generate evolution files
-o, --conform            Generate conforming triangulation
-s N, --steiner=N        Maximum number of Steiner points for
                         conforming triangulation (default is no limit)
-t, --slope              Maximisation of minimum slope
-q Q, --quality=Q        Set the minimum acceptable face quality
-a A, --area=A           Set the maximum acceptable face area
-v, --verbose            Print statistics about the triangulation
-h, --help               Display this help and exit

-n N, --number=N     Stop the coarsening process if the number of edges
                     was to fall below N.
-c C, --cost=C       Stop the coarsening process if the cost of collapsing
                     an edge is larger than C.
-m, --midvertex      Use midvertex as replacement vertex instead of default
                     volume optimized point.
-l, --length         Use length^2 as cost function instead of default
                     optimized point cost.
-f F, --fold=F       Set maximum fold angle to F degrees (default 1 degree).
-w W, --vweight=W    Set weight used for volume optimization (default 0.5).
-b W, --bweight=W    Set weight used for boundary optimization (default 0.5).
-s W, --sweight=W    Set weight used for shape optimization (default 0.0).
-p, --progressive    Write progressive surface file.
-L, --log            Log the evolution of the cost.
-f VAL, --fold=VAL   Smooth only folds.
-v, --verbose        Print statistics about the surface.
-h, --help           Display this help and exit.


AUTHOR
======

The GTS library was written by Stéphane Popinet <popinet@users.sourceforge.net>.
The pyformex-delaunay command is taken from the examples.
