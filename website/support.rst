..    -*- rst -*-

..
  This file is part of the pyFormex project.
  pyFormex is a tool for generating, manipulating and transforming 3D
  geometrical models by sequences of mathematical operations.
  Home page: http://pyformex.org
  Project page:  https://savannah.nongnu.org/projects/pyformex/
  Copyright (C) Benedict Verhegghe (benedict.verhegghe@ugent.be)
  Distributed under the GNU General Public License version 3 or later.


  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/.



.. include:: <isonum.txt>
.. include:: defines.inc
.. include:: links.inc

.. index:: Support

Support
=======

`Support tracker`_
------------------
If you need help with the installation or use of pyFormex, or if you want to
discuss existing features or request new ones, the best way is to use the
pyFormex `Support tracker`_.

.. index:: Issues

`Issue tracker`_
----------------
If you run into a problem that looks like a malfunction or a bug in pyFormex,
or if you want to suggest improvements or additions to the pyFormex code,
you can create the `Issue tracker`_ on the gitlab development pages.

.. End
