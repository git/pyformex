..   -*- rst -*-
  
..
  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
  SPDX-License-Identifier: GPL-3.0-or-later
  
  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
  pyFormex is a tool for generating, manipulating and transforming 3D
  geometrical models by sequences of mathematical operations.
  Home page: https://pyformex.org
  Project page: https://savannah.nongnu.org/projects/pyformex/
  Development: https://gitlab.com/bverheg/pyformex
  Distributed under the GNU General Public License version 3 or later.
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/.
  
  
.. _`email us`: mailto:bverheg@gmail.com
.. _`pyFormex website`: https://pyformex.org
.. _`Home Page`: https://pyformex.nongnu.org
.. _`Documentation-3.0`: https:///pyformex.nongnu.org/doc-3.0
.. _`Documentation-2.0`: https:///pyformex.nongnu.org/doc-2.0
.. _`Documentation-1.0`: https:///pyformex.nongnu.org/doc-1.0
.. _`Documentation-0.9`: https:///pyformex.nongnu.org/doc-0.9

.. _`Installing pyFormex-3.4`: https:///pyformex.nongnu.org/doc-3.4/install.html
.. _`Installing pyFormex-2.0`: https:///pyformex.nongnu.org/doc-2.0/install.html
.. _`Installing pyFormex-1.0`: https:///pyformex.nongnu.org/doc-1.0/install.html
.. _`Installing pyFormex-0.9`: https:///pyformex.nongnu.org/doc-0.9/install.html

.. _`Online manual`: https:///pyformex.nongnu.org/doc/index.html
.. _`Introduction to pyFormex`: https:///pyformex.nongnu.org/doc/intro.html
.. _`pyFormex tutorial`: https:///pyformex.nongnu.org/doc/tutorial.html
.. _`pyFormex reference manual`: https:///pyformex.nongnu.org/doc/refman.html
.. _`pyFormex user guide`: https:///pyformex.nongnu.org/doc/user-guide.html
.. _`pyFormex examples`: https:///pyformex.nongnu.org/doc/examples.html
.. _`PDF version`: https://download.savannah.gnu.org/releases/pyformex/pyformex-2.0.pdf

.. _`Savannah`: https://savannah.nongnu.org/
.. _`Project page`: https://savannah.nongnu.org/projects/pyformex/
.. _`Support tracker`: https://savannah.nongnu.org/support/?group=pyformex
.. _`Bug tracker`: https://savannah.nongnu.org/bugs/?group=pyformex
.. _`Git repository`: https://savannah.nongnu.org/git/?group=pyformex
.. _`News`: https://savannah.nongnu.org/news/?group=pyformex
.. _`Releases`: https://download.savannah.gnu.org/releases/pyformex/
.. _`latest`: https://download.savannah.gnu.org/releases/pyformex/pyformex-latest.tar.gz
.. _`Development repository`: https://gitlab.com/bverheg/pyformex
.. _`Issue tracker`: https://gitlab.com/bverheg/pyformex/-/issues

.. _`Python`: https://python.org
.. _`Python documentation`: https://python.org/doc
.. _`Python tutorial`: https://docs.python.org/3/tutorial
.. _`Python setuptools`: https://pypi.org/project/setuptools/
.. _`Python wheel`: https://pypi.org/project/wheel/
.. _`NumPy`: http://numpy.org
.. _`PyOpenGL`: http://pyopengl.sourceforge.net
.. _`Qt5`: http://doc.qt.io
.. _`Qt4`: https://doc.qt.io/archives/
.. _`PySide2`: http://wiki.qt.io/Qt_for_Python
.. _`PySide`: http://www.pyside.org
.. _`PyQt5`: http://www.riverbankcomputing.co.uk/software/pyqt/
.. _`PyQt4`: http://www.riverbankcomputing.co.uk/software/pyqt/
.. _`PIL`: http://python-pillow.org
.. _`git`: http://git-scm.com/
.. _`Subversion`: http://subversion.tigris.org
.. _`Sphinx`: http://sphinx.pocoo.org
.. _`reStructuredText`: http://docutils.sourceforge.net/rst.html
.. _`GIMP`: http://www.gimp.org

.. _`GNU GPL`: http://www.gnu.org/licenses/gpl.html
.. _`GNU General Public License`: http://www.gnu.org/licenses/gpl.html
.. _`Free Software Foundation`: http://fsf.org
.. _`Debian GNU/Linux`: http://www.debian.org
.. _`Debian Live Wiki`: http://wiki.debian.org/DebianLive/Howto/USB

.. _`dd for Windows`: http://www.chrysocome.net/dd

.. _`stent research group`: http://www.stent-ibitech.ugent.be
.. _`IBiTech-bioMMeda`: http://www.biommeda.ugent.be
.. _`IBiTech, Ghent University`: http://www.ibitech.ugent.be
.. _`BuMPer cluster`: http://bumps.ugent.be/bumper
.. _`FEops`: http://www.feops.com

.. _`Vascular Modeling Toolkit`: http://www.vmtk.org

.. End
