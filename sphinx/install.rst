..
  
..
  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
  SPDX-License-Identifier: GPL-3.0-or-later
  
  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
  pyFormex is a tool for generating, manipulating and transforming 3D
  geometrical models by sequences of mathematical operations.
  Home page: https://pyformex.org
  Project page: https://savannah.nongnu.org/projects/pyformex/
  Development: https://gitlab.com/bverheg/pyformex
  Distributed under the GNU General Public License version 3 or later.
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/.
  
  
.. include:: defines.inc
.. include:: links.inc


.. _cha:install:

*******************
Installing pyFormex
*******************

.. topic:: Abstract

   This document explains the different ways for obtaining a running
   pyFormex-3.4 installation. You will learn how to download pyFormex, how
   to install it, and how to get it running.

.. note:: If you are an experienced Linux and pyFormex user, you may
	  directly go to the :ref:`sec:quick-guide`.


.. _sec:introduction:

Introduction
============
This installation manual is for the pyFormex 3.4 version only. Methods and
tools for packaging and installing Python applications are currently evolving
quickly. For a complex application like pyFormex, with a long legacy and
many involved external packages, a nonstandard installation procedure
is required. This installation manual is therefore limited to some typical
use cases. It describes the requirements for pyFormex, how to download
the pyFormex-3.4 source and how to install that source so you can run it.

pyFormex is being developed on GNU/Linux systems and currently only runs on
Linux. On other systems your best option is to run Linux in a virtual machine
or boot your machine from a USB stick with a Linux Live system.

pyFormex is software under continuous development. It can be run
directly from the development sources and some users are indeed doing that.
It holds a certain risk however, because the development version may at times
be unstable or highhly incompatible with previous versions and thus break your
applications.  At regular times we therefore create official releases,
which provide a more stable and better documented and supported
version, together with an easy install procedure.

If you can meet the requirements for using an officially packed
release, that is the recommended way to install pyFormex.
All the software packages needed to run pyFormex are open source and are
freely available

.. _sec:requirements:

Requirements
============
To install an official pyFormex release, you need a working GNU/Linux
system. First, make sure that the :ref:`sec:dependencies` listed
below are installed on the system. If you need to install some of them,
you will need to have  root privileges to the system (whether through `su`
or `sudo`) or to ask your system manager to install them dfor you.
Then preceed to :ref:`sec:official-release`.

If you need to install a new GNU/Linux system from scratch, and have
the choice to pick any distribution, we highly recommend `Debian
GNU/Linux`_ or derivatives.  This is because most of the pyFormex
development is done on Debian systems, and below you'll find precise
instructions to `install dependencies on Debian`_.  Also, the Debian software
repositories are amongst the most comprehensive to be found on the
Internet. We recommend Debian 11, Debian 12, Ubuntu 22.04, or similar
derivatives.

.. _sec:dependencies:

Dependencies
============

.. note:: If your Linux system is Debian 11, Debian 12, Ubuntu 22.04 or derived
          from these, you may opt to directly go to
	  `install dependencies on Debian`_.

Most popular GNU/Linux distributions provide appropriately packed
recent versions of the dependencies, so that you can install them
easily from the pacakge manager of your system. In case a package or
version is not available for your system, you can always install it
from source. We provide the websites where you can find the source
packages.

Whether you install an official release package of pyFormex, or you run
pyFormex from the development source tree, you need to have the following
installed (and working) on your computer:

`Python`_
  Version 3.8 or higher, 3.9 or higher is recommended. Nearly all
  GNU/Linux distributions come with Python installed. Some old installations
  may however still have Python2.x installed as the default Python, so you may
  have to install the Python3 version.
  Usually, the Python 3.x executable is named 'python3',
  and pyFormex expects it that way. To check your version, do::

    python3 --version

`Python setuptools`_

`Python wheel`_

`NumPy`_
  Version 1.16 or higher, 1.19 recommended. NumPy is the package used for
  efficient numerical array operations in Python and is essential for pyFormex.

`PIL`_
  The Python Imaging Library from the pillow fork, used for loading and
  saving images in lots of formats.

`Qt5`_ or Qt6
  Qt is the widget toolkit on which the pyFormex Graphical User
  Interface (GUI) is built. We recommended Qt5, but the newer Qt6 version
  should also work if you have the proper Python3 bindings

`PySide2`_ or `PyQt5`_ or PyQt6 or PySide6
  These are Python bindings for the Qt5 (or Qt6) libraries.
  We recommend Pyside2 if the licensing suits your use case.

`PyOpenGL`_
   Python bindings for OpenGL, used for drawing and manipulating the
   3D-structures.

admesh
   Used to check and fix STL files. We need the binary, not the Python library.

libgts-0.7-5
   A library for operating on triangulated surfaces.

To compile the acceleration library (highly recommended!), you will also
need the appropriate Python and OpenGL header files, GNU make and the GNU
compiler gcc:

- make
- gcc
- python3-dev
- libglu1-mesa-dev

Furthermore, we recommend to install the following for extended functionality:

- python3-gnuplot or python3-matplotlib
- python3-pydicom
- python3-docutils
- python3-scipy
- units
- imagemagick
- tetgen
- libdxflib3

.. _`install dependencies on Debian`:

Installing dependencies on Debian and alikes
............................................

On `Debian GNU/Linux`_ systems (and Debian-derivatives like Ubuntu)
you can install all basic prerequisites and recommended packages and
all their dependencies with the following command (executed in the
unpacked pyFormex source, see :ref:`sec:official-release`)::

  sudo ./install_deps_apt


.. _sec:official-release:

Install an official pyFormex release
====================================

.. _sec:downloading:

Download pyFormex
.................
Official releases of pyFormex are distributed in .tar.gz (tarball) format.
They can be downloaded from `Releases`_. As of the writing of this manual,
the latest release is |latest|. You can download the latest version
with the command::

  wget https://download.savannah.nongnu.org/releases/pyformex/pyformex-latest.tar.gz

Alternatively, you can use the specific version filename:
pyformex-|latest|.tar.gz.
After downloading, you have to unpack the tarball with the command::

   tar xf pyformex-VERSION.tar.gz

where you replace ``VERSION`` to match the downloaded file.
This will create a subdirectory ``pyformex-VERSION`` containing all the
source files.

.. _sec:building:

Build pyFormex
..............
Go into the directory where you unpacked the pyFormex source::

   cd pyformex-VERSION

and execute the following command::

   ./install.sh build

This will build pyFormex, compile its acceleration libraries and some external
utility programs, and collect all needed files into a wheel format archive
placed under the ``build`` subdirectory. If the command ::

   ls build

shows a file name like ``pyformex-3.4-cp311-cp311-linux_x86_64.whl``, you are
ready to install its contents.

.. _sec:installing:

Install pyFormex
................
Installing pyFormex is done with a command like::

    [ sudo ] ./install.sh [-s] [-S] [-d] install

Before doing the install, you have to make a few decisions on how and where
the program should be installed.

Install as user or as root
~~~~~~~~~~~~~~~~~~~~~~~~~~
Running the installation procedure as a normal (non-root) user will install
pyFormex under the user's home directory. The user will have access to all
the files with the ability to change these as he wishes. Other users on the
same system will normally not be able to run the installed pyFormex.

Running the installation procedure as root (e.g. using ``sudo``), will install
pyFormex under /usr/local and it will become available to all users on the
system, but they will not be able to modify the program (as normal user).

Install as multiversion or single version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
By default the installation procedure allows multiple pyFormex versions to be
installed in parallel. The ``pyformex`` package is then installed in a folder
``pyformex-VERSION`` in the installation path and the pyFormex executable is
also called ``pyformex-VERSION``. One of the installed versions can be made
the default: that version will then be executed if you simply enter ``pyformex``.
That will also be the imported package if you do ``import pyformex`` in Python.
The -d option of the install.sh procedure will set the version being installed
as the default.

If you prefer to allways install just a single version, use the -s option
on the install procedure. If existing pyFormex installations are detected,
you will be asked if they may be deleted before installation of the new one.
An option -S will force such deletion without asking (use with care).
The -s/-S option installs pyFormex just like in a multiple versions system.
It implies the -d option, making this single version the default. You
can later add other versions if you wish to do so.

Common installation examples
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
As user::

    ./install.sh install

As user, making the new version the default::

    ./install.sh -d install

Systemwide, making the new version default::

    sudo ./install.sh -d install

Systemwide, single version install::

    sudo ./install.sh -s install

Systemwide, single version install, silently remove other versions::

    sudo ./install.sh -S install

Check installation
~~~~~~~~~~~~~~~~~~
If the installation succeeded, you can find out where pyFormex is installed
with the command ::

   pyformex --whereami

Use ``pyformex-VERSION`` if you didn't make it the default.
Next you can run ::

   pyformex --detect

to give you a list of installed and detected software that pyFormex is able to
use as helpers. Check under 'Installed System' that you have
pyFormex_installtype (R) or (U) and
pyFormex_libraries (``pyformex.lib.misc_c``, ``pyformex.lib.nurbs_c``).
The gts, gts-bin and gts-extra packages do not have a version, and just
display 'gts' if they are installed.

You can find out what pyFormex versions are installed in the target
install path with::

   pyformex -V

.. _sec:cleanup:

Cleanup installation files
..........................
After installation you may want to cleanup the temporary files
generated during the build/install::

    ./install.sh clean

Use sudo if you also did the build with sudo.

You can also safely remove the unpacked source directory
and even the downloaded targz::

    cd ..
    rm -rf pyformex-VERSION
    rm pyformex-VERSION.tar.gz

.. _sec:remove:

Uninstall pyFormex
..................
When pyFormex is installed by the procedure above, it can be removed by
executing the command ::

   pyformex --remove

and answering 'yes' to the question. You may want to do this before
installing a new version if you do not want to remove all the existing
versions.

.. _sec:quick-guide:

Quick installation guide
========================

Quick installation from official .tar.gz release or from git clone:

1. Install dependencies. On recent Debian/Ubuntu you can just run::

     sudo ./install_deps_apt

2. Install pyFormex. This can be done either in user space, or system-wide
   (as root)::

     ./install.sh all

   or ::

     sudo ./install.sh all

   In the first case, the executable is installed under $HOME/.local/bin.
   For the system-wide installation, the executable is in /usr/local/bin.

   As of version 2.7 of pyFormex, the default installation allows for
   multiple versions being installed in parallel, and the executable is
   named pyformex-VERSION, where VERSION is the version designator.

   Add a '-d' to the install.sh options to make the installed version
   the default by linking this executable under the generic name 'pyformex'.

   Add a '-s' or '-S' to the install.sh options to create a single version
   installation. Older versions are removed after user acknowledgment
   (with -s) or silently (with -S). This option implies '-d'.

3. (Optional) Remove the temporary build directory with::

     ./install.sh clean

   Add 'sudo' if you also used it to build.


.. _sec:source-tree:

Running pyFormex from sources
=============================
If the officially released pyFormex packages are not suitable for your needs,
and you can not find proper packages for your distribution, you can try
running pyFormex directly from the source in the git repository.
Besides the :ref:`sec:dependencies` for the official release, you will also
need to have git installed.

The source code can be downloaded anonymously from the `Git repository`_ or
the `Development repository`_. Any of these commands::

  git clone https://git.savannah.nongnu.org/git/pyformex.git MYDIR
  git clone https://gitlab.com/bverheg/pyformex.git MYDIR

will checkout the source to a local directory ``MYDIR``. The first repository
contains more stable versions.

Provided you have all the prerequisites installed, pyFormex can then be run
directly from the checked out source with the command::

  MYDIR/pyformex/pyformex

If you want to use the compiled accelerator library however, you will have to
create it first::

  cd MYDIR
  make lib

Once you have a checked-out source tree, you can easily sync it to the latest
repository version by just issuing the following command from your checkout
directory::

  git pull

.. End
