#
##
##  SPDX-FileCopyrightText: © 2007-2023 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.4  (Thu Nov 16 18:07:39 CET 2023)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##

"""Unit tests for the pyformex.fileread/write/ module

These unit test are based on the pytest framework.

"""

# TODO: it would be better to remove this from pytest
# or we need to setup pytest to properly startup pyFormex

import os
import pyformex as pf
import numpy as np

from pyformex.fileread import *
from pyformex.filewrite import *
from pyformex.pzffile import *
from pyformex.mesh import Mesh

M = Mesh(eltype='quad4').convert('tri3-u')
d = {'mesh': M}

pf.Path('test/work').mkdir(parents=True)
if pf.verbosity(1):
    print("\n===> Writing uncompressed files")
    print(f"\n===> curdir is {os.getcwd()}")

writePZF('test/work/filewrite.pzf', d)
writePYF('test/work/filewrite.pyf', d)
writeHDF5('test/work/filewrite.hdf5', d)
writePGF('test/work/filewrite.pgf', d)
writeOFF('test/work/filewrite_mesh.off', M)
writeOFF('test/work/filewrite_coords_elems.off', M.coords, M.elems)
writeOFF('test/work/filewrite_coords_elemlist.off', M.coords, [M.elems])
writeOBJ('test/work/filewrite_mesh.obj', M)
writeOBJ('test/work/filewrite_coords_elems.obj', M.coords, M.elems)
writeOBJ('test/work/filewrite_coords_elemlist.obj', M.coords, [M.elems])
writePLY('test/work/filewrite_mesh.ply', M)
writePLY('test/work/filewrite_coords_elems.ply', M.coords, M.elems)
writePLY('test/work/filewrite_coords_elemlist.ply', M.coords, [M.elems])
writePLY('test/work/filewrite_binary.ply', M, binary=True)
writeGTS('test/work/filewrite.gts', M.toSurface())
writeSTL('test/work/filewrite.stl', M.toFormex())
writeSTL('test/work/filewrite_binary.stl', M.toFormex(), binary=True)

if pf.verbosity(1):
    print("\n===> Writing compressed files")

writePGF('test/work/filewrite.pgf.gz', [M])
writeOFF('test/work/filewrite.off.gz', M)
writeOBJ('test/work/filewrite.obj.gz', M)
writePLY('test/work/filewrite.ply.gz', M)
writeGTS('test/work/filewrite.gts.gz', M.toSurface())
writeSTL('test/work/filewrite.stl.gz', M.toFormex())
writeSTL('test/work/filewrite_binary.stl.gz', M.toFormex(), binary=True)

if pf.verbosity(1):
    print("\n===> Reading back uncompressed files")

readPZF('test/work/filewrite.pzf')
readPYF('test/work/filewrite.pyf')
#readHDF5('test/work/filewrite.hdf5')  # Currently does not work
readPGF('test/work/filewrite.pgf')
readOFF('test/work/filewrite_mesh.off')
readOFF('test/work/filewrite_coords_elems.off')
readOFF('test/work/filewrite_coords_elemlist.off')
readOBJ('test/work/filewrite_mesh.obj')
readOBJ('test/work/filewrite_coords_elems.obj')
readOBJ('test/work/filewrite_coords_elemlist.obj')
readPLY('test/work/filewrite_mesh.ply')
readPLY('test/work/filewrite_coords_elems.ply')
readPLY('test/work/filewrite_coords_elemlist.ply')
readPLY('test/work/filewrite_binary.ply')
readGTS('test/work/filewrite.gts')
readSTL('test/work/filewrite.stl')
readSTL('test/work/filewrite_binary.stl')

if pf.verbosity(1):
    print("\n===> Reading back compressed files")

readPGF('test/work/filewrite.pgf.gz')
readOFF('test/work/filewrite.off.gz')
readOBJ('test/work/filewrite.obj.gz')
readPLY('test/work/filewrite.ply.gz')
readGTS('test/work/filewrite.gts.gz')
readSTL('test/work/filewrite.stl.gz')
readSTL('test/work/filewrite_binary.stl.gz')

# End
