..

pyFormex v4 goals
-----------------
pyFormex version 4 will have some important design changes.
Our goals are:

- Make pyFormex ready for the future: Python 3.12, PyQt6/PySide6, NumPy2
- Provide a cleaner user environment in the style of NumPy.
  So the user can do::

    import pyformex as pf

  and access all basic and important functionality from there.
- Avoid and discourage the use of * imports.
- define a pyFormex core language with a stable public API.
  All the definitions in the pyformex module are guaranteed to be
  supported and changes to the public API will be slow and documented.
- Applications will need much less imports and moving definitions
  or modules to other places will be effortless.
- Allow more and faster changes to pyFormex without impacting the user
  (because of less imports).
- Add more integration between the different pyFormex components.

The details
-----------
To achieve the goals the main pyformex module has to import all the
relevant pyFormex modules and all the important function and class definitions.
The pyformex module (which we'll henceforth call ``pf``, referring to its alias)
is the first one imported on startup and because it is used for communicating
global data between the modules, it is also imported in most of the other
modules. Simply importing all the relevant definitions into ``pf`` would create
circular dependencies. Therefore, the imports are collected into a special
module and are only imported from there into ``pf`` after the other modules have
been loaded.

Because pyFormex can be run in GUI or nonGUI mode, we have to do this in
two separate steps. The ``pyformex.core`` module collects all the nonGUI
definitions. They are imported into ``pf`` at the end of basic startup,
before the GUI is started. The relevant GUI definitions are collected
in a module ``pyformex.gui.guicore``, and its contents is added to ``pf``
at the end of GUI startup.

The benefits:

- Avoid all '*' imports. This avoids name clashes and allows the use
  of tools (like flake8) to check application code for forgotten or unused
  imports and unused variables. For user applications it means no longer use
  ``from pyformex.script import *`` or ``from pyformex.gui.draw import *``.
- Avoid the need for lots of imports to get to all the important user
  oriented functionality. Therefore, all the important core definitions
  (GUI and nonGUI) are imported in the pyformex module.
  Thus, most everything is accessible by doing
  ``import pyformex as pf``. Clearly, we are following the NumPy style here,
  where everything is got with ``import numpy as np``. Henceforth we will
  use the prefered alias ``pf`` for the pyformex module.
- All user oriented core modules are imported into ``pf``. Moreover, all the
  important definitions from these modules are also directly imported in ``pf``.
  Thus, ``pf.mesh`` is the ``pyformex.mesh`` module and ``pf.Mesh`` is short
  for the ``pyformex.mesh.Mesh`` class. Other functions in that module,
  like rectangleWithHole, require the full form ``pf.mesh.rectangleWithHole``,
  but are still accessible without importing anything.
- The core module defines what is imported in ``pf``, and the gui.guicore module
  defines what is additionally imported in ``pf`` when the GUI is loaded. T
- Define a stable public API. This is to ensure stability of the pyFormex
  platform for the long term. Changes to the public API will then be limited,
  announced and gradual, with clear upgrade paths.
  For now, we define the pyFormex public API as everything in the ``pf``
  module if

  - the name does not start with an underscore, OR
  - the name is contained in the ``_public_`` attribute of either the
    ``pf.core`` or the ``pf.gui.guicore`` module.

  This definition may be refined later.

Alternatives
------------
We have initially explored (and for now rejected) an alternative where the
nonGUI and GUI core languages were kept separate. Thus one could do::

  import pyformex as pf
  import pyformex.gui as pg

and then access all nonGUI stuff from ``pf`` and GUI things from ``pg``.
We finally ended up mergeing the two together, because we felt the
distinction ``pf`` or ``pg`` to confusing, and it needs an extra import
statement. If one wants to have the distinction it is still possible to
do ``import pyformex.gui.guicore as pg``.


Other changes
-------------
Project
.......
- decoupled the concept Project from a saved project file. Allows for an
  increased use and importance of the Project class
- preferential Project save format is now .pzf, though .pyf will continue
  to be supported. (Actually, objects that have no own .pzf format are saved
  in .pyf format inside the .pzf container.
- a Project manager has been added to interactively manage and view the
  contents of a project. There is a lot of room for additional functionality.
  It can be started from the Project menu, or by clicking on the Project
  button in the status bar.
- since saving Geometry is also preferentially done in .pzf format, the
  Project manager can directly be used to manage Geometry as well

FE modules
..........
Have been moved to a subpackage ``pf.fe``

Progress
--------
In order to achieve the above goals, we have to shuffle around and change a lot
of code, avoiding circular imports. The changes are huge, and are for now kept
in the branch 'benedict' of the repository. Just do ``git pull`` and
``git checkout benedict`` to experiment with it. Most of the stuff is working.
Some menus (Geometry and following) still need some adaptation. Mostly,
the selection stuff in the Geometry menu is broken, as it will be merged with the
new Project Manager.

We will wait with merging the development branch into the master branch until
the remainder has been fixed and users have made feedback (see further).

Upgrade path
------------
Migation from pyFormex 3.5 to pyFormex 4 will be made as easy as possible.
Changes to the functionality are limited. And a high level of backward
compatibility can be obtained by importing everything from ``pf``.
The ``pyformex.gui.draw`` module is still available (but deprecated).
So if you use ``from pyformex.gui.draw import *``, most should work unaltered.
We strongly recommend however (and certainly for new code) to replace that
with ``import pyformex as pf`` and prefix every pyFormex symbol  with ``pf.``.
It will take a while to get used (I still occasionally type ``draw`` instead
of ``pf.draw``), but you'll benefit in the end. I have detected and fixed many
bugs thanks to the ability of using linter tools.
All the examples have been transformed, so you can have a look at these to see
the impact on the code.

Note that the ``pyformex`` module is automatically imported as ``pf`` when
you are running a script/app, so for small scripts you can just start without
any import.


How you can (and should) help
-----------------------------
- Test and see what is not working (correctly)
- Make suggestions for what needs to or could be added to the pyFormex core
  language.
- Discuss the changed concept of pyFormex


.. End
