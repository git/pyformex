#!/usr/bin/env python3
##
##  SPDX-FileCopyrightText: © 2007-2024 Benedict Verhegghe <bverheg@gmail.com>
##  SPDX-License-Identifier: GPL-3.0-or-later
##
##  This file is part of pyFormex 3.5  (Thu Feb  8 19:11:13 CET 2024)
##  pyFormex is a tool for generating, manipulating and transforming 3D
##  geometrical models by sequences of mathematical operations.
##  Home page: https://pyformex.org
##  Project page: https://savannah.nongnu.org/projects/pyformex/
##  Development: https://gitlab.com/bverheg/pyformex
##  Distributed under the GNU General Public License version 3 or later.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see http://www.gnu.org/licenses/.
##
#
"""manifest.py

This script creates the list of files to be included in
the pyFormex source distribution.
"""
from importlib.machinery import SourceFileLoader
path = SourceFileLoader("path","pyformex/path.py").load_module()
from path import Path

def listTree(path, **kargs):
    # pkgs = [path] + [f"{path}/{d}" for d in kargs.get('includedir', [])]
    # PKGS.extend([d.replace('/','.') for d in pkgs])
    return Path(path).listTree(**kargs)

def paths(l):
    return [Path(f) for f in l]

VERSION = '4.0'
PYFVER = 'pyformex-' + VERSION

UNTRACKED_FILES = paths([
    PYFVER+'.desktop',
    PYFVER+'.png',
    'pyformex/doc/pyformex.1',
])

# Distribution meta files
META_FILES = paths([
    'README.rst',
    'LICENSE',
    'Description',
    'RELEASE',
    'install.sh',
    'install_desktop_file',
    'install_deps_apt',
    'ReleaseNotes',
    # 'manifest.py',
    'setup.py',
    # 'setup.cfg',
])

# Essential pyFrmex files
PYF_FILES = (
    listTree('pyformex',
             includedir=['gui','menus','plugins','opengl',
                         'fe','apps'],   # DO NOT ADD scripts HERE
             includefile=['.*\\.py$','pyformex(rc)?$','pyformex.conf$'],
             excludefile=[],
             ) +
    listTree('pyformex/freetype',
             includefile=['.*\\.py$'],
             ) +
    listTree('pyformex/icons', ftypes='f',
             excludedir=['64x64.rejected'],
             includefile=['README','.*\\.xpm$','.*\\.png$','.*\\.gif$']
             ) +
    listTree('pyformex/glsl', ftypes='f', includefile=['.*\\.c',]) +
    listTree('pyformex/fonts', includefile=['.*\\.png',]) +
    listTree('pyformex/lib',
             includefile=['.*\\.h$','.*\\.c$','.*\\.py$','.*\\.pyx$']
             )
)

# Examples and scripts
EXAMPLE_FILES = (
    listTree('pyformex/examples',
             includedir=[],
             excludefile=['.*\\.pyc','.*~$',],
             includefile=['[_A-Z].*\\.py$','apps.cat','README']) +
    listTree('pyformex/scripts',
             includedir=['userguide'],
             includefile=['README', '.*\\.py',]) +
    listTree('pyformex/bin', excludefile=['.*~$'])
)

# pyFormex data files
DATA_FILES = listTree(
    'pyformex/data',
    includedir=[],
    excludefile=['.*\\.pyc','.*~$','PTAPE.*'],
    includefile=[
        'README',
        'benedict_6\\.jpg',
        'bifurcation\\.off.gz',
        'blippo\\.pgf',
        'blippok\\.ttf',
        'butterfly\\.png',
        'cookies\\.txt',
        'fewgl\\.js',
        'hesperia-nieve\\.prop',
        'horse\\.off',
        'horse\\.pgf',
        'image_not_loaded\\.png',
        'man\\.off',
        'man\\.pgf',
        'mark_cross\\.png',
        'materials\\.json',
        'sections\\.json',
        'splines\\.pgf',
        'supershape\\.txt',
        'teapot1\\.off',
        'template.py',
        'world\\.jpg',
        'x11_webcolors.txt',
        'xkcd_webcolors.txt',
        'xtk_xdat\\.gui\\.js',
    ],
)

# pyFormex documentation files
DOC_FILES = (
    listTree('pyformex/doc', maxdepth=0, ftypes='f',
             includefile=[
                 'README.rst',
                 'Description',
                 'LICENSE',
                 'ReleaseNotes',
             ]) +
    listTree('pyformex/doc/html', ftypes='f')
)

# scripts to install extra programs
EXTRA_FILES = listTree(
    'pyformex/extra',
    includedir=[
        'calpy',
        'dxfparser',
        'gts',
        'instant',
        'postabq',
        'pygl2ps',
        ],
    excludefile=['.*~$', 'example.post.py'],
    includefile=[
        'README',
        'Makefile',
        '.*\\.sh$',
        '.*\\.rst$',
        '.*\\.patch$',
        '.*\\.c$',
        '.*\\.cc$',
        '.*\\.h$',
        '.*\\.i$',
        '.*\\.py$',
        '.*\\.1$',
        '.*\\.pc$',
        ],
    )


TRACKED_FILES =  (
    META_FILES +
    PYF_FILES +
    EXAMPLE_FILES +
    DATA_FILES +
    DOC_FILES +
    EXTRA_FILES
)

DIST_FILES = UNTRACKED_FILES + TRACKED_FILES

PKGS = sorted(set([f.parent for f in DIST_FILES]))  # paths
PKGS = [p.replace('/', '.') for p in PKGS[1:]]  # pkg format,



def fix_setup():
    # install the PKGS in setup.py
    setup = Path('setup.py')
    lines = []
    copy = True
    for line in setup.read_lines():
        if copy:
            lines.append(line)
            if line.startswith('PKGS ='):
                copy = False
                for pkg in PKGS:
                    lines.append(f"    '{pkg}',")
        else:
            if line.startswith(']'):
                lines.append(line)
                copy = True
    setup.write_lines(lines)
    print("Update setup.py")



def create_manifest():
    # write the manifest file
    with open('MANIFEST.in', 'w') as fil:
        fil.write('\ninclude '.join(['prune *\nexclude *'] + DIST_FILES))
        fil.write('\n')
    print("Created new MANIFEST.in")
    # check for changes in packages
    pkglist = Path('PKGLIST')
    if pkglist.exists():
        pkgs = list(pkglist.read_lines())
    else:
        pkglist.write_text('')
        pkgs = []
    if pkgs != PKGS:
        pkglist.write_lines(PKGS)
        if pkgs:
            print(f'{pkglist=} has changed, saving')
        else:
            print(f'Create new {pkglist=}')
    fix_setup()



options = {
    'pkgs': 'PKGS',
    'meta': 'META_FILES',
    'pyf': 'PYF_FILES',
    'example': 'EXAMPLE_FILES',
    'data': 'DATA_FILES',
    'doc': 'DOC_FILES',
    'extra': 'EXTRA_FILES',
    'dist': 'DIST_FILES',
}

if __name__ == '__main__':
    import sys

    todo = sys.argv[1:]
    if not todo:
        todo = ['usage']

    for a in todo:
        option = options.get(a, 'usage')
        data = globals().get(option, None)
        if data:
            print(f"========= {option} =========")
            print('\n'.join(data))
        elif a == 'manifest':
            create_manifest()
        else:
            print("""\
Usage: manifest.py CMD [...]
where CMD is one of:
  pkgs: list the packages
  meta: list the distribution meta files
  pyf: list the pyFormex essential files
  example: list the examples
  data: list the data files
  doc: list the doc files
  extra: list the extra files
  dist: list ALL files to go in the dist (tar.gz)
  manifest: write the MANIFEST.in file
""")



# End
